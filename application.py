from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from PIL import Image, ImageTk
import os
import mosse
import sys
import csv
import traceback


DEBUG_MODE = False


class MainWindow:
    "main window of application"
    def __init__(self, master, casc_func, ev_mode):
        self.root = master
        self.reusable_data = None
        self.ladder_name_full = ""
        self.chosen_file_full = ""
        self.last_mode = ""
        self.ev_mode = ev_mode
        self.casc_func = casc_func
        self.root.title("Gel electrophoresis analysis")
        self.current_ladder = [None, None]
        self.disable = False
        self.target_folder_full = ""
        fs = list()
        for i in range(5):
            fs.append(Frame(self.root))
            if i == 0:
                fs[i].pack()
            else:
                fs[i].pack(side=TOP)
            if i == 4:
                fs[i].config(height=60)
            else:
                fs[i].config(height=4)

        self.output_picture = None
        self.information = None

        self.input_data = None
        self.output_data = None
        self.chosen_file = StringVar()
        self.target_folder = StringVar()

        menubar = Menu(self.root)

        filemenu = Menu(self.root, tearoff=0)
        filemenu.add_command(label="Open Image", command=self.choose_file)
        filemenu.add_command(label="Select targer folder", command=self.choose_folder)
        filemenu.add_command(label="Save results", command=self.save_data)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.close_app)
        menubar.add_cascade(label="File  ", menu=filemenu)

        lmenu = Menu(self.root, tearoff=0)
        lmenu.add_command(label="Create ladder file  ", command=self.set_ladder)
        lmenu.add_command(label="Select ladder file  ", command=self.select_ladder_file)
        lmenu.add_command(label="Select BP file  ", command=self.select_ladder_file_2)
        menubar.add_cascade(label="Ladder  ", menu=lmenu)

        self.root.config(menu=menubar)

        self.mode = StringVar()
        self.mode.set("MOSSE")
        linfo = Label(fs[0], text="Detection\nmode:")
        linfo.pack(side=LEFT)
        linfo.config(height=2, padx=10)
        option = OptionMenu(fs[0], self.mode, "MOSSE", "ML")
        option.pack(side=LEFT)
        option.config(width=12)
        self.mode_in = StringVar()
        pinfo = Label(fs[0], text="Base pairs\nmode:")
        pinfo.pack(side=LEFT)
        pinfo.config(height=2, padx=10)
        self.mode_in.set("Hyperbolic")
        option2 = OptionMenu(fs[0], self.mode_in, "Hyperbolic", "Linear Intrp")
        option2.pack(side=LEFT)
        option2.config(width=15)
        dinfo = Label(fs[0], text="Threshold\npercentage:")
        dinfo.pack(side=LEFT)
        dinfo.config(height=2, padx=10)
        self.th = Scale(fs[0],from_=10, to=250, orient=HORIZONTAL)
        self.th.set(100)
        self.th.config(length=140)
        self.th.pack(side=LEFT)
        self.c = IntVar()
        check = Checkbutton(fs[0], text="Custom threshold", variable=self.c)
        check.pack(side=LEFT)

        sinfo = Label(fs[1],text="Image: ")
        sinfo.pack(side=LEFT)
        sinfo.config(height=2, width=15)
        source = Label(fs[1],textvariable=self.chosen_file,anchor="w")
        source.pack(side=LEFT)
        source.config(height=2, width=35)
        ssinfo = Label(fs[1],text="Ladder file: ")
        ssinfo.pack(side=LEFT)
        ssinfo.config(height=2, width=15)
        self.ladder_name = StringVar()
        self.ladder_name_full = ""
        saveinfo = Label(fs[1], textvariable=self.ladder_name)
        saveinfo.pack(side=LEFT)
        saveinfo.config(height=2, width=35)
        tinfo = Label(fs[2],text="Folder: ")
        tinfo.pack(side=LEFT)
        tinfo.config(height=2, width=15)
        target = Label(fs[2],textvariable=self.target_folder, anchor="w")
        target.pack(side=LEFT)
        target.config(height=2, width=48)
        self.sn = StringVar()
        saveinfo = Label(fs[2], text="Filename: ")
        saveinfo.pack(side=LEFT)
        saveinfo.config(height=2, width=15)
        savename = Entry(fs[2], textvariable=self.sn, width=25)
        savename.pack(side=LEFT)

        btn3 = Button(fs[3], text="Analyse image", command=self.analyse_image)
        btn3.pack(side=LEFT)
        btn3.config(height=2)
        ilabel = Label(fs[3], text="Output image")
        ilabel.config(font="BOLD",width=20,height=3)
        ilabel.pack(side=LEFT)

        self.exp_button = Button(fs[3], text="Expand\nimage", command=self.expand_image)
        self.exp_button.pack(side=LEFT)
        self.exp_button.config(height=2,width=10)
        self.exp_button["state"] = "disabled"

        self.canvas_width = 700
        self.canvas_height = 400
        self.canvas = Canvas(fs[4], width=self.canvas_width, height=self.canvas_height)
        self.canvas.pack()

    def set_ladder(self):
        "create new window in order to set new ladder format"
        if self.disable:
            return
        new_window = Toplevel(self.root)
        app = SetLadderWindow(new_window)
        self.disable = True
        self.root.wait_window(new_window)
        self.disable = False

    def analyse_image(self):
        "starts image analysis, if all conditions are met"
        if self.disable:
            return
        if not os.path.exists(self.chosen_file_full):
            messagebox.showerror(title="Error", message="File not chosen or Invalid file")
        else:
            self.exp_button["state"] = "disabled"
            if self.current_ladder[0] is None:
                messagebox.showwarning(title="Warning", message="Default ladder pattern is used")
            self.output_data = None
            self.output_picture = None
            p = self.chosen_file_full
            path = os.path.dirname(p)
            filename = os.path.basename(p)
            bs_mode = 1
            if self.mode_in.get() == "Hyperbolic":
                bs_mode = 0
            data = None

            if self.mode.get() == self.last_mode:
                data = self.reusable_data
            tm = 100
            if self.c.get() == 1:
                tm = self.th.get()
            else:
                self.th.set(100)
            if DEBUG_MODE:
                print("Selected analysis mode: {}".format(self.mode.get()))
                print("USE threshold mult: {}".format(self.c.get()))
                print("Treshold multiplier: {}".format(tm))
            if self.mode.get() == "ML":
                if self.casc_func is None:
                    if DEBUG_MODE:
                        print("ML disabled by flag")
                    messagebox.showwarning(title="Error", message="ML mode was disabled by sys argument")
                    return
                (pil, data), self.reusable_data = self.casc_func(path, filename, self.current_ladder, bs_mode, self.ev_mode, data, tm)
            else:
                (pil, data), self.reusable_data = mosse.detection_app(path, filename, self.current_ladder, bs_mode, self.ev_mode, data, tm)
            self.last_mode = self.mode.get()
            if DEBUG_MODE:
                print("Image analysis successful")
            self.output_picture = pil
            self.output_data = data
            self.include_output(pil)

            self.exp_button["state"] = "normal"

    def save_data(self):
        "save image and output file to target folder"
        if self.disable:
            return
        if not os.path.exists(self.target_folder_full) or len(self.target_folder_full) < 3:
            messagebox.showerror(title="Error", message="Target folder not chosen")
            return
        elif self.output_picture is None:
            messagebox.showerror(title="Error", message="No data processed")
        else:
            name = self.sn.get()
            if "." in name:
                messagebox.showerror(title="Error", message="Invalid filename")
                return
            try:
                path = os.path.join(self.target_folder_full, self.sn.get() + ".png")
                path2 = os.path.join(self.target_folder_full, self.sn.get() + ".csv")
                self.output_picture.save(path, "PNG")
                self.csv_write_data(path2)
                self.output_data = None
                self.output_picture = None
                if DEBUG_MODE:
                    print("Data successfully written")
                messagebox.showinfo(title="Info", message="Image and data saved")
            except:
                if DEBUG_MODE:
                    print("Saving data problem:")
                    traceback.print_exc()
                messagebox.showerror(title="Error", message="Invalid filename chosen")

    def csv_write_data(self, path):
        "write output as CSV"
        with open(path, mode='w') as employee_file:
            csv_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow(['Line index', 'Band index', 'No. of Base pairs'])
            for i in range(len(self.output_data)):
                for j in range(1, len(self.output_data[i])):
                    csv_writer.writerow([str(i+1), str(j), str(self.output_data[i][j])])

    def close_app(self):
        self.root.destroy()

    def expand_image(self):
        "expand image to its full resolution"
        if self.output_picture is None:
            messagebox.showerror(title="Image error", message="No image loaded")
            return
        self.output_picture.show()

    def include_output(self, PIL_image):
        "show output image in main window"
        if self.disable:
            return
        self.canvas.delete("all")
        if PIL_image is None:
            return
        pil = PIL_image
        (w,h) = pil.size
        if w > h:
            pil = pil.resize((self.canvas_width,int(self.canvas_width * h / w)))
        else:
            pil = pil.resize((int(self.canvas_height * w / h), self.canvas_height))
        self.img = ImageTk.PhotoImage(pil)
        self.canvas.create_image(self.canvas_width//2, self.canvas_height//2, anchor="center", image=self.img)

    def select_ladder_file(self):
        "select and check ladder file"
        if self.disable:
            return
        fname = filedialog.askopenfilename(title="Choose an .dat or file with ladder information",
                                           filetypes=[('dat', '.dat')], initialdir = os.getcwd())
        if fname:
            try:
                f = open(fname,"r")
                ladder = [[], []]
                valid = True
                for line in f:
                    k = line.split()
                    if len(k) != 2:
                        valid = False
                        break
                    for i in range(len(k)):
                        if not k[i].isdigit() or int(k[i]) < 0:
                            valid = False
                            break
                        else:
                            ladder[i].append(int(k[i]))
                f.close()
                if not valid:
                    messagebox.showerror("Invalid Ladder File", "Invalid ladder file selected\n")
                else:
                    ladder[0].sort()
                    ladder[1].sort()
                    if len(ladder[0]) != len(ladder[1]):
                        ladder[1] = None
                    self.current_ladder = ladder
                    self.ladder_name_full = fname
                    self.ladder_name.set(os.path.basename(fname))
            except:
                if DEBUG_MODE:
                    print("ERROR: Select ladder type error")
                    traceback.print_exc()
                messagebox.showerror("Open Ladder File", "Failed to read file\n'%s'" % fname)
                self.ladder_name.set("")
                self.current_ladder = [None, None]
        else:
            self.ladder_name.set("")
            self.current_ladder = [None, None]
        return

    def select_ladder_file_2(self):
        "select and check file with pattern values only"
        if self.disable:
            return
        fname = filedialog.askopenfilename(title="Choose an .txt file with ladder information",
                                           filetypes=[('txt', '.txt')], initialdir=os.getcwd())
        if fname:
            try:
                f = open(fname, "r")
                ladder = [[], []]
                valid = True
                for line in f:
                    k = line.split()
                    if len(k) != 1:
                        valid = False
                        break
                    for i in range(len(k)):
                        if not k[i].isdigit() or int(k[i]) < 0:
                            valid = False
                            break
                        else:
                            ladder[i].append(int(k[i]))
                f.close()
                if not valid:
                    messagebox.showerror("Invalid BP File", "Invalid Base pairs file selected\n")
                else:
                    ladder[0].sort()
                    ladder[1].sort()
                    if len(ladder[0]) != len(ladder[1]):
                        ladder[1] = None
                    self.current_ladder = ladder
                    self.ladder_name_full = fname
                    self.ladder_name.set(os.path.basename(fname))
            except:
                if DEBUG_MODE:
                    print("ERROR: Select Base pairs file type error")
                    traceback.print_exc()
                messagebox.showerror("Open BP File", "Failed to read file\n'%s'" % fname)
                self.ladder_name.set("")
                self.current_ladder = [None, None]
        else:
            self.ladder_name.set("")
            self.current_ladder = [None, None]
        return

    def choose_folder(self):
        "choose target folder"
        if self.disable:
            return
        fname = filedialog.askdirectory()
        if fname:
            try:
                self.target_folder_full = fname

                path, file = os.path.split(fname)
                folders = [file]
                l = len(file)
                to_add = False
                while 1:
                    if l > 40:
                        to_add = True
                        break
                    path, folder = os.path.split(path)
                    if folder != "":
                        folders.append(folder)
                        l += len(folder)
                    else:
                        if path != "":
                            folders.append(path)
                            l += len(path)
                            break
                    if len(path) < 1 or (len(path) < 2 and ("\\" in path or "/" in path)):
                        break
                folders = folders[::-1]
                path = folders[0]
                for i in range(1, len(folders)):
                    path = os.path.join(path, folders[i])

                if to_add:
                    if "\\" in path:
                        path = "...\\" + path
                    else:
                        path = ".../" + path
                self.target_folder.set(path)
            except:
                if DEBUG_MODE:
                    print("ERROR: Selecting folder:")
                    traceback.print_exc()
                messagebox.showerror("Target folder", "Failed to read target path\n'%s'" % fname)
            return

    def choose_file(self):
        "choose input image"
        self.reusable_data = None
        if self.disable:
            return
        fname = filedialog.askopenfilename(title="Choose an image",
            filetypes=[
               ('jpg/png', '.png'),
               ('jpg/png', '.jpg'),
           ])
        if DEBUG_MODE:
            print("Selected filename: {}".format(fname))
        if fname:
            try:
                pil = Image.open(fname).convert("RGB")
                self.chosen_file_full = fname
                self.chosen_file.set(os.path.basename(fname))
                self.set_filename(fname)
                self.include_output(pil)
                self.output_data = None
                self.output_picture = None
                self.exp_button["state"] = "disabled"
            except:
                if DEBUG_MODE:
                    print("ERROR: Choosing file:")
                    traceback.print_exc()
                messagebox.showerror("Open Input Image File", "Failed to read image file\n'%s'" % fname)
            return

    def set_filename(self, fname):
        "set first variant of to-save-filename"
        if self.disable:
            return
        name = os.path.basename(fname)
        index = name.find(".")
        self.sn.set(name[:index] + "_analyzed")


class SetLadderWindow:
    "window for setting new ladder format"
    def __init__(self, master):
        self.master = master
        self.xs, self.ys, self.circles = [], [], []
        self.values = []
        f0 = Frame(self.master)
        f0.pack()
        f0.config(height=4)
        f1 = Frame(self.master)
        f1.pack(side=TOP)
        f1.config(height=4)
        f2 = Frame(self.master)
        f2.pack(side=TOP)
        btn0 = Button(f1, text="Select text file", command=self.select_txt)
        btn0.pack(side=LEFT)
        btn0.config(height=2, width=15)
        self.btn1 = Button(f1, text="Select image", command=self.select_image, state="disabled")
        self.btn1.pack(side=LEFT)
        self.btn1.config(height=2, width=15)
        self.btn_text = StringVar()
        self.btn_text.set("---")
        self.btn2 = Button(f1, textvariable=self.btn_text, command=self.save_all, state="disabled")
        self.btn2.pack(side=LEFT)
        self.btn2.config(height=2, width=15)
        self.img = None
        self.canvas_dim = 550
        self.canvas = Canvas(f2, width=self.canvas_dim, height=self.canvas_dim, borderwidth=0)
        self.canvas.grid(row=0, column=0)

        label = Label(f0, text="Filename:")
        label.pack(side=LEFT)
        label.config(height=2)
        self.sn = StringVar()
        savename = Entry(f0, textvariable=self.sn, width=50)
        savename.pack(side=LEFT)

    def select_txt(self):
        "select text file with base pairs"
        self.img = None

        self.canvas.unbind("<Button-1>")
        self.canvas.delete("all")
        fname = filedialog.askopenfilename(title="Choose an txt file",
                                           filetypes=[
                                               ('txt', '.txt')
                                           ])

        self.btn1["state"], self.btn2["state"] = "disabled", "disabled"
        self.values = []
        if fname:
            try:
                f = open(fname, mode="r")
                for i in f:
                    if (i.rstrip()).isnumeric() and int(i) > 0:
                        self.values.append(int(i))
                    else:
                        messagebox.showerror("ValueError", "Wrong data format or negative integer")
                        f.close()
                        return
                if len(self.values) < 5:
                    messagebox.showerror("ValueError", "Not enough data in file")
                    f.close()
                    return
                self.btn1["state"] = "normal"
                self.btn_text.set(str(len(self.values)))
                f.close()
            except:
                if DEBUG_MODE:
                    print("ERROR: Selecting txt file:")
                    traceback.print_exc()
                messagebox.showerror("Open Source File", "Failed to read file\n'%s'" % fname)
            return

    def select_image(self):
        "select image and perform automatic detection"
        self.img = None
        self.xs, self.ys = [], []
        self.canvas.unbind("<Button-1>")
        self.canvas.delete("all")
        self.btn2["state"] = "disabled"
        fname = filedialog.askopenfilename(title="Choose an image",
            filetypes=[
               ('jpg/png', '.png'),
               ('jpg/png', '.jpg'),
           ])

        if fname:
            try:
                path = os.path.dirname(fname)
                filename = os.path.basename(fname)
                extremes, pil = mosse.automatic_ladder_generation(path, filename, len(self.values))
                (w, h) = pil.size
                if w > h:
                    pil = pil.resize((self.canvas_dim, int(self.canvas_dim * h / w)))
                    coeffs = [self.canvas_dim / w, self.canvas_dim / w]
                else:
                    pil = pil.resize((int(self.canvas_dim * w / h), self.canvas_dim))
                    coeffs = [self.canvas_dim / h, self.canvas_dim / h]

                self.img = ImageTk.PhotoImage(pil)
                self.canvas.create_image(self.canvas_dim//2, self.canvas_dim//2, anchor="center", image=self.img)
                for i in extremes:
                    self.xs.append(int((i[0] - w//2) * coeffs[0]) + self.canvas_dim // 2)
                    self.ys.append(int((i[1] - h//2) * coeffs[1]) + self.canvas_dim // 2)
                    self.circles.append(self.create_circle(self.xs[-1], self.ys[-1],4))
                if len(self.xs) == len(self.values):
                    self.btn2["state"] = "normal"
                    self.btn_text.set("SAVE")
                else:
                    self.btn2["state"] = "disabled"
                    self.btn_text.set(str(len(self.values) - len(self.xs)))
                self.canvas.bind("<Button 1>", self.save_coordinates)

            except:
                if DEBUG_MODE:
                    print("ERROR: Selecting image for ladder definition:")
                    traceback.print_exc()
                messagebox.showerror("Open Source File", "Failed to read file\n'%s'" % fname)
            return

    def create_circle(self, x, y, radius):
        "put circles in image to signal chosen positions"
        return self.canvas.create_oval(x - radius*1.5, y - radius, x + radius*1.5, y + radius, fill="green")

    def save_coordinates(self, event):  # function called when left-mouse-button is clicked
        "save position of click, if circle is being clicked, remove it"
        x = event.x  # save x and y coordinates selected by the user
        y = event.y
        used = False
        for i in range(len(self.xs)):
            if ((x - self.xs[i]) / 1.5) **2 + (y-self.ys[i])**2 < 16:
                self.xs.pop(i)
                self.ys.pop(i)
                c = self.circles.pop(i)
                self.canvas.delete(c)
                used = True
                break
        if not used:
            if len(self.xs) == len(self.values):
                pass
            else:
                self.xs.append(x)
                self.ys.append(y)
                self.circles.append(self.create_circle(x,y,4))
        if len(self.xs) == len(self.values):
            self.btn2["state"] = "normal"
            self.btn_text.set("SAVE")
        else:
            self.btn2["state"] = "disabled"
            self.btn_text.set(str(len(self.values) - len(self.xs)))

    def save_all(self):
        "save ladder format and close window"
        self.ys.sort()
        self.values.sort()
        self.values = self.values[::-1]
        try:
            if len(self.sn.get()) > 3:
                s = self.sn.get() + ".dat"
            else:
                s = "ladder.dat"
                messagebox.showwarning(title="Warning", message="Filename not selected\nDefault filename selected")
            f = open(s, mode="w")
            for i in range(len(self.values)):
                s = str(self.values[i]) + " " + str(self.ys[i]) + "\n"
                f.write(s)
            f.close()
            self.canvas.unbind("<Button-1>")
            self.close_window()
        except:
            if DEBUG_MODE:
                print("ERROR: Saving ladder:")
                traceback.print_exc()
            messagebox.showerror(title="Error", messagebox="Invalid filename")

    def close_window(self):
        self.master.destroy()


def main():
    casc_flag = False
    grb_flag = False
    for i in range(len(sys.argv)):
        if sys.argv[i] == "-d":
            global DEBUG_MODE
            DEBUG_MODE = True
        if sys.argv[i] == "-c":
            casc_flag = True
        if sys.argv[i] == "-g":
            grb_flag = True
    casc_func = None
    if not casc_flag:
        import cascade_filter
        casc_func = cascade_filter.detection_app
    ev_mode = "GRB"
    if grb_flag:
        ev_mode = "NO"

    root = Tk()
    app = MainWindow(root, casc_func, ev_mode)
    root.mainloop()


if __name__ == "__main__":
    main()


