import numpy as np
import scipy.optimize as opt
import matplotlib.pyplot as plt
import picture
import time
import gurobipy as grb


LADDER_TYPE_0 = [128, 132, 136, 143, 152, 166, 180,  202, 214, 240, 253, 267, 285, 309, 321]
LADDER_TYPE_0_VALUES = [50, 100, 200, 300, 400, 500, 1000, 1400, 1550, 2000, 3000, 4000, 6000, 8000, 10000]
LADDER_TYPE_0_VALUES = LADDER_TYPE_0_VALUES[::-1]

LADDER_TYPE_1 = [72, 80, 90, 107, 120, 142, 160,  184, 200, 221, 232, 243, 256, 271]
LADDER_TYPE_1_VALUES = [100, 200, 300, 400, 500, 600, 750, 1400, 1550, 2000, 3000, 4000, 6000, 8000]
LADDER_TYPE_1_VALUES = LADDER_TYPE_1_VALUES[::-1]

LADDER_TYPE_2 = [121, 134, 149, 177, 203, 243, 275, 325, 356, 401, 425, 448, 484, 524]
LADDER_TYPE_2_VALUES = [10000,8000,6000,4000,3000,2000,1500,1200,1000,750,500,400,300,200]

PATTERNS = [LADDER_TYPE_0, LADDER_TYPE_1, LADDER_TYPE_2]
P_VALUES = [LADDER_TYPE_0_VALUES, LADDER_TYPE_1_VALUES, LADDER_TYPE_2_VALUES]

# optimal LAMBDA parameters for all variants of algorithm
LAMBDA_LH_1 = 10
LAMBDA_LH_2 = 100

LAMBDA_H_1 = 10
LAMBDA_H_2 = 100


class BandSolver:
    "class used to evaluate unknown bands"
    def __init__(self, pattern, num_values, xs, ys, mode=1):
        self.num_values = num_values
        self.pattern = pattern
        self.xs = xs
        self.ys = ys
        self.coeffs = None
        self.new_ys, self.value_function_params = self.find_polynomial()
        if mode==0:  # use hyperbolic function to estimate number of base pairs
            self.eval_fnc = self.evaluate_band_hyp
        else:  # use linear interpolation to estimate number of base pairs
            self.eval_fnc = self.evaluate_band_lin_intrp

    def find_polynomial(self):
        "computes corrective polynomial"
        if self.ys.shape[1] == 1:
            A = np.zeros((self.ys.shape[0],2))
        elif self.ys.shape[1] == 2:
            A = np.zeros((self.ys.shape[0] * 2,4))
        else:
            A = np.zeros((self.ys.shape[0] * self.ys.shape[1], 5))
        b = np.zeros(self.ys.shape[0] * self.ys.shape[1])
        for i in range(self.ys.shape[1]):
            for j in range(self.ys.shape[0]):
                x, y = self.xs[i], self.ys[j,i]
                A[i*self.ys.shape[0] + j,:] = np.array([1, y, x, x*y, x**2])[:A.shape[1]]  # set type of polynomial
                b[i*self.ys.shape[0] + j] = self.pattern[j]
        self.coeffs = np.linalg.pinv(A) @ b
        data = np.zeros(self.ys.shape[0] * (self.ys.shape[1] + 1))
        for i in range(data.shape[0]):
            if i < self.ys.shape[0]:
                data[i] = self.pattern[i]
            else:
                i_x = i // self.ys.shape[0] - 1
                i_y = i % self.ys.shape[0]
                data[i] = self.corrective_function(self.xs[i_x], self.ys[i_y,i_x])
        self.new_ys = data
        return data, self.find_value_function_coefficients(data)

    def visualize_global_hyperbolic_function(self):
        "visualize fitted hyperbolic function and reference ladders"
        k = []
        p = list(self.new_ys[:len(self.num_values)])
        p.insert(0,self.new_ys[0]-2)
        p.append(self.new_ys[-1]+5)
        for i in range(len(p)):
            k.append(self.value_function(p[i]))
        plt.plot(p, k, color="blue", linewidth=2, label="Regresní křivka - varianta 1")
        colors = ["red", "green", "yellow", "black"]
        labels = ["Referenční žebříček", "Žebříček "]
        for i in range(self.new_ys.shape[0] // self.ys.shape[0]):
            if i != 0:
                label = labels[1] + str(i)
            else:
                label = labels[0]
            plt.scatter(self.new_ys[i * self.ys.shape[0]:(i + 1) * self.ys.shape[0]], self.num_values, color=colors[i],
                        label=label)
        plt.xlabel("y")
        plt.ylabel("počet bází")
        plt.legend()
        plt.show()

    def evaluate_band_hyp(self, point):
        "evaluate band while using hyperbolic function"
        (x, y) = point
        new_y = self.corrective_function(x, y)  # compute corrected coordinate
        val = self.value_function(new_y)  # estimate number of base pairs
        return val

    def evaluate_band_lin_intrp(self, point):
        "evaluate band while using linear interpolation"
        (x,y) = point
        new_y = self.corrective_function(x, y)  # compute corrected coordinate
        lower, index = False, -1
        for i, val in enumerate(self.pattern):  # find indices that surround corrected coordinate
            if new_y < val:
                if i == 0:
                    return self.num_values[0]
                else:
                    lower, index = True, i
                break
        if not lower:
            return self.num_values[-1]
        # return estimation based on linear interpolation
        return (new_y - self.pattern[i-1]) / (self.pattern[i] - self.pattern[i-1]) * (self.num_values[i] - self.num_values[i-1]) + self.num_values[i-1]

    def corrective_function(self, x, y):
        "compute corrected coordinate by fitted polynomial"
        W = [1, y, x, x*y, x**2]
        new_y = 0
        for i in range(len(self.coeffs)):
            new_y += W[i] * self.coeffs[i]
        return new_y

    def value_function(self, x):
        "function returns number of base pairs by using hyperbolic function"
        (a, b, c) = list(self.value_function_params)
        val = a / (x - b) + c
        if x < b:
            return max(self.num_values) + 100
        elif val < 0:
            return 0
        return val

    def visualize_corrective_polynomials(self, array):
        "function for visualization of corrective polynomials"
        r, g, b = np.copy(array), np.copy(array), np.copy(array)
        for i in range(array.shape[1]):
            for j in range(array.shape[0]):
                y = self.corrective_function(i,j)
                for s in self.pattern:
                    if abs(y-s) < 1.8:
                        r[j,i], g[j,i], b[j,i] = 255, 0, 0
        for i in range(self.ys.shape[1]):
            for j in range(self.ys.shape[0]):
                for s in range(-3,4):
                    for t in range(-3,4):
                        x = int(self.xs[i] + s)
                        y = int(self.ys[j,i] + t)
                        if not picture.is_in_image(x, y, array.shape):
                            continue
                        r[y, x], g[y, x], b[y, x] = 0, 255, 0
        rgbs = np.array([r, g, b]).astype(int).transpose([1, 2, 0]).astype('uint8')
        plt.imshow(rgbs)
        plt.show()

    def find_value_function_coefficients(self, data, test=False):
        "function finding LSQ optimized hyperbolic function parameters"
        def criterion_function(x):
            # used when nb of base pairs is independent
            k = np.array(
                [x[0] / (self.num_values[i % self.ys.shape[0]] - x[2]) + x[1] - data[i] for i in range(data.shape[0])])
            return k

        def criterion_function_2(x):
            # used when nb of base pairs is dependent on position
            k = np.array([x[0] / (data[i] - x[1]) + x[2] - self.num_values[i % self.ys.shape[0]] for i in range(data.shape[0])])
            return k

        def criterion_jac(x):
            k = []
            for i in range(data.shape[0]):
                val = self.num_values[i % self.ys.shape[0]]
                k.append((1 / (val - x[2]), 1, x[0]/(val-x[2])**2))
            return np.array(k)

        def criterion_jac_2(x):
            k = []
            for i in range(data.shape[0]):
                val = data[i]
                k.append((1 / (val - x[1]), x[0] / (val - x[1])**2, 1))
            return np.array(k)

        if test:  # compute parameters of function from criterion_function_2
            x_0_2 = np.array([10000, min(self.pattern)-100,min(self.num_values)-100])
            bounds_2 = ((0, -np.inf, -np.inf), (np.inf, self.pattern[0],min(self.num_values)))
            res2 = opt.least_squares(criterion_function_2, x_0_2, bounds=bounds_2, jac=criterion_jac_2)
            return res2.x
        else:
            x_0 = np.array([10, min(self.pattern) - 100, min(self.num_values)-100])
            bounds = ((0, -np.inf, -np.inf), (np.inf, self.pattern[0],min(self.num_values)))
            res = opt.least_squares(criterion_function, x_0, bounds=bounds, jac=criterion_jac)
            return res.x

    def return_data_for_visualisation(self):
        "support function used to visualize ladders in final image"
        intern_offsets = []
        ladder_points = []
        for i in range(self.xs.shape[0]):
            intern_offsets.append((int(self.ys[0,0] - self.ys[0,i]), int(self.xs[i])))
            intern_offsets[-1] = (0, int(self.xs[i]))
            for j in range(self.ys.shape[0]):
                ladder_points.append((int(self.xs[i]), int(self.ys[j,i] + intern_offsets[i][0])))
        return intern_offsets, ladder_points

    def evaluate_group(self, group):
        "functions evaluates bands belonging to same lane"
        avg_x = 0
        values = []
        for point in group:
            avg_x += point[0]
            values.append(int(self.eval_fnc(point)))
        x = int(avg_x / len(group))
        offset = 0
        anotated_points = []
        for i, point in enumerate(group):
            anotated_points.append((x, int(group[i][1] + offset), int(values[i])))
        return anotated_points, (offset, x)


def compute_criterion_through_hungarian_extended(ladder, pattern, coeffs, LAMBDA, L2=False):
    "function solves assigment problem with hungarian method"
    A = np.zeros((len(ladder), len(pattern) + len(ladder)))
    for i in range(len(ladder)):
        for j in range(len(pattern)):
            if L2:
                A[i,j] = abs(-ladder[i] + coeffs[0] + coeffs[1] * pattern[j]) ** 2#
            else:
                A[i, j] = abs(-ladder[i] + coeffs[0] + coeffs[1] * pattern[j])
    right_side = np.ones((len(ladder), len(ladder))) * 1e+30
    np.fill_diagonal(right_side, LAMBDA)
    A[:,len(pattern):] = right_side
    row_ind, col_ind = opt.linear_sum_assignment(A)
    sort_assigned(col_ind, len(pattern))
    J = A[row_ind, col_ind].sum()
    col_ind[col_ind >= len(pattern)] = -1
    return J, col_ind


def sort_assigned(config, m=-1):
    "reorganize computed assignment"
    indices = []
    vals = []
    lc = len(list(config))
    for i in range(lc):
        if (m > 0 and config[i] < m) or (m==-1 and config[i] > -1):
            indices.append(i)
            vals.append(config[i])
    for i in range(len(indices)-1, -1, -1):
        mx, ix = -1, -1
        for j in range(i+1):
            if vals[j] > mx:
                mx, ix = vals[j], j
        vals[i], vals[ix] = vals[ix], vals[i]
    for i in range(len(indices)):
        config[indices[i]] = vals[i]
        
    
def compute_criterion_for_constant_assignment_grb(data, assignment, pattern, LAMBDA):
    "finds best coefficients of affine transform, when L1 norm is used, using gurobi solver"
    n = len(assignment)
    c = np.ones(2+n)
    c[0], c[1] = 0, 0
    opt_model = grb.Model(name="name")
    opt_model.setParam('OutputFlag', False)
    opt_model.ModelSense = grb.GRB.MINIMIZE
    x_vars = {0 : opt_model.addVar(ub=grb.GRB.INFINITY, lb=-grb.GRB.INFINITY,vtype=grb.GRB.CONTINUOUS, name="alpha")}
    x_vars[1] = opt_model.addVar(ub=grb.GRB.INFINITY, lb=-0,vtype=grb.GRB.CONTINUOUS, name="beta")
    for i in range(n):
        x_vars[i+2] = opt_model.addVar(ub=grb.GRB.INFINITY, lb=0,vtype=grb.GRB.CONTINUOUS, name=str(i))
    unused = 0
    constraints = dict()
    for i in range(n):
        index = int(2 * i)
        if assignment[i] > -1:
            coeff = pattern[int(assignment[i])]
            constraints[index] = opt_model.addConstr(lhs=(x_vars[0] + coeff * x_vars[1] - x_vars[2+i]), rhs=data[i],
                                                     sense=grb.GRB.LESS_EQUAL, name=str(index))
            constraints[index] = opt_model.addConstr(lhs=(-x_vars[0] - coeff * x_vars[1] - x_vars[2 + i]), rhs=-data[i],
                                                     sense=grb.GRB.LESS_EQUAL, name=str(index+1))
        else:
            c[2+i] = 0
            unused += 1
    objective = grb.quicksum(c[i] * x_vars[i] for i in range(2+n))
    opt_model.setObjective(objective)
    opt_model.optimize()
    fun = opt_model.getObjective()
    fun = fun.getValue()
    fun += LAMBDA * unused
    return fun, (x_vars[0].x, x_vars[1].x)


def compute_criterion_for_constant_affine_grb(ladder, pattern, coeffs, opt_model, x_vars, LAMBDA, L2=False):
    "compute criterion for constant affine function using gurobi solver"
    n = len(ladder)
    m = len(pattern)
    c = np.zeros(m * n + m + n)
    p = 1
    if L2:
        p = 2
    for i in range(m * n):
        z_i = i // m
        z_j = i % m
        c[i] = abs(-ladder[z_i] + coeffs[0] + coeffs[1] * pattern[z_j]) ** p
    c[m * n:m * n + n] = LAMBDA
    objective = grb.quicksum(x_vars[i] * c[i] for i in range(m*n+m+n))
    opt_model.ModelSense = grb.GRB.MINIMIZE
    opt_model.setObjective(objective)
    opt_model.optimize()
    cfg = []
    xs = list(opt_model.getVars())
    for i in range(n):
        used = False
        for j in range(m):
            val = xs[i*m + j].x
            if val > 0:
                cfg.append(j)
                used = True
                break
        if not used:
            cfg.append(-1)
    l = cfg.copy()
    sort_assigned(cfg)
    J = 0
    for i in range(len(cfg)):
        if cfg[i] == -1:
            J += LAMBDA
        else:
            J += c[m*i + cfg[i]]
    return J, np.array(cfg)


def compute_criterion_for_constant_affine(ladder, pattern, coeffs, A_eq, b_eq, LAMBDA, L2=False):
    "solves LP relaxed assignment problem by interior point method in scipy.linprog"
    n = len(ladder)
    m = len(pattern)
    c = np.zeros(m*n + m + n)
    p = 1
    if L2:
        p = 2
    for i in range(m*n):
        z_i = i // m
        z_j = i % m
        c[i] = abs(-ladder[z_i] + coeffs[0] + coeffs[1] * pattern[z_j]) ** p
    c[m*n:m*n+n] = LAMBDA
    res = opt.linprog(c, A_ub=None, b_ub=None, A_eq=A_eq, b_eq=b_eq, bounds=(0,1), options={'tol': 1e-8},method="interior-point")
    x, fun = res.x, res.fun
    x = np.array(x.astype(float))
    x = recover_config(x, m, n)
    return fun, x


def L2_correction(ladder, cfg, pattern, LAMBDA):
    "finds best coefficients of affine transform, when L2 norm is used"
    n = len(ladder)
    A = np.zeros((n, 2))
    b = np.zeros(n)
    unused = 0
    for i in range(n):
        if cfg[i] == -1:
            A[i, 0], A[i, 1] = 0, 0
            b[i] = 0
            unused += 1
        else:
            A[i,0], A[i,1] = 1, pattern[int(cfg[i])]
            b[i] = ladder[i]
    x = np.linalg.pinv(A) @ b
    return ((A @ x - b) ** 2).sum() + LAMBDA * unused, x


def create_model(m, n):
    "create model for assignment optimization"
    number = m*n +m + n
    A_eq = np.zeros((m + n, m * n + m + n))
    for i in range(n):
        A_eq[i, i * m:(i + 1) * m], A_eq[i, m * n + i] = 1, 1
    for i in range(m):
        for j in range(n):
            A_eq[n + i, m * j + i] = 1
        A_eq[n + i, m * n + n + i] = 1
    opt_model = grb.Model(name="Model")
    opt_model.setParam( 'OutputFlag', False)
    x_vars = {i:opt_model.addVar(vtype=grb.GRB.CONTINUOUS, ub=1, lb=0, name="z{0}".format(i)) for i in range(number)}
    constraints = {j: opt_model.addConstr(lhs=grb.quicksum(A_eq[j, i] * x_vars[i] for i in range(number)),
            sense=grb.GRB.EQUAL,
            rhs=1,
            name="constraint_{0}".format(j))
        for j in range(m+n)}
    opt_model.ModelSense = grb.GRB.MINIMIZE
    return opt_model, constraints, x_vars


def vis_x(x, m, n):
    "function used for assignment visualization"
    print(x)
    for i in range(n):
        for j in range(m):
            print(x[m*i+j], end=" ")
        print()


def LoRANSAC_approach(ladder, pattern, LAMBDA, HUN=True, L2=False, iters=None, inlier=0.3, timer=1):
    "algorithm for fiting ladder, based on Lo-RANSAC"
    n = len(ladder)
    m = len(pattern)
    model, cons, vars = create_model(m,n)
    A_eq, b_eq, prob_dist, prob_dist_2 = create_matrices_for_LP_and_probs(m,n) # matrices are used in scipy.linprog
    pattern = np.array(pattern)
    k1, k2 = np.zeros(m + 1), np.zeros(m + 1)  # used to determine limit in RANSAC for inliers
    k1[1:], k2[:m] = pattern, pattern
    J_min, best_coeffs, best_config = np.inf, (0,0), np.ones((1,n)) * (-1)
    inlier_threshold = np.min(np.abs(k1-k2)) * inlier
    if iters is None:
        RANSAC_iters = 1200
    else:
        RANSAC_iters = iters
    best_inliers = 0
    #TEST
    a=time.time()
    for i in range(RANSAC_iters): #RANSAC iters
        inliers = 0
        while True:
            guess = np.random.choice(n, 2, p=prob_dist)
            if guess[0] == guess[1]:
                continue
            else:
                if guess[0] > guess[1]:
                    guess = guess[::-1]
                min_d = guess[1] - guess[0]
                break
        while True:
            paste = np.random.choice(m, 2, p=prob_dist_2)
            if abs(paste[0] - paste[1]) < min_d // 4:
                continue
            else:
                if paste[0] > paste[1]:
                    paste = paste[::-1]
                break
        A = np.array([[1, pattern[paste[0]]], [1, pattern[paste[1]]]])
        b = np.array([ladder[guess[0]], ladder[guess[1]]])
        coeffs = np.linalg.pinv(A) @ b
        if coeffs[1] < 0.1: # save switch against small coeffs pushing all bands to one point
            continue
        for w in range(n):
            if np.min(np.abs(ladder[w] - coeffs[0] - coeffs[1] * pattern)) < inlier_threshold * coeffs[1]:
                inliers += 1
        if inliers > 0.75 * best_inliers:
            last_coeffs = coeffs
            for p in range(20):
                if HUN:
                    J, cfg = compute_criterion_through_hungarian_extended(ladder, pattern, coeffs, LAMBDA, L2)
                else:
                    J, cfg = compute_criterion_for_constant_affine_grb(ladder, pattern,coeffs, model, vars, LAMBDA, L2)
                if L2:
                    J2, coeffs2 = L2_correction(ladder, cfg, pattern, LAMBDA)
                else:
                    J2, coeffs2 = compute_criterion_for_constant_assignment_grb(ladder, cfg, pattern, LAMBDA)
                coeffs = coeffs2
                J = J2
                if coeffs[1] == last_coeffs[1] or p == 19:
                    break
                else:
                    last_coeffs = coeffs
            acc_inliers = np.sum(cfg>=0)
            if acc_inliers > best_inliers:
                best_inliers = acc_inliers
            if J < J_min:
                J_min, best_coeffs, best_config = J, coeffs, cfg
    if iters is None:
        return J_min, [best_coeffs, best_config]
    return J_min, [best_coeffs, best_config], time.time() - a


def RANSAC_estimation(m, n, prob_1, prob_2, pattern, ladder, tolerance, RN_iters=None):
    "function estimating affine transform parameters by RANSAC algorithm"
    best_estimate, best_inliers = (0,1), 0
    if RN_iters is None:
        RANSAC_iters = 150
    else:
        RANSAC_iters = RN_iters
    for i in range(RANSAC_iters):
        inliers = 0
        while True:
            guess = np.random.choice(n, 2, p=prob_1)
            if guess[0] == guess[1]:
                continue
            else:
                if guess[0] > guess[1]:
                    guess = guess[::-1]
                min_d = guess[1] - guess[0]
                break
        while True:
            paste = np.random.choice(m, 2, p=prob_2)
            if abs(paste[0] - paste[1]) < min_d // 4:
                continue
            else:
                if paste[0] > paste[1]:
                    paste = paste[::-1]
                break
        A = np.array([[1, pattern[paste[0]]], [1, pattern[paste[1]]]])
        b = np.array([ladder[guess[0]], ladder[guess[1]]])
        coeffs = np.linalg.pinv(A) @ b
        if coeffs[1] < 0.1:
            continue
        for w in range(n):
            if np.min(np.abs(ladder[w] - coeffs[0] - coeffs[1] * pattern)) < tolerance * coeffs[1]:
                inliers += 1
        if inliers > best_inliers:
            best_inliers, best_estimate = inliers, coeffs
    return best_estimate


def create_matrices_for_LP_and_probs(m,n):
    "support function creating parabolic probability distributions and bound matrices for LP relaxed problem"
    A_eq = np.zeros((m + n, m * n + m + n))
    for i in range(n):
        A_eq[i, i * m:(i + 1) * m], A_eq[i, m * n + i] = 1, 1
    for i in range(m):
        for j in range(n):
            A_eq[n + i, m * j + i] = 1
        A_eq[n + i, m * n + n + i] = 1
    b_eq = np.ones(m + n)
    prob_dist = np.ones(n)
    prob_dist /= np.sum(prob_dist)
    prob_dist_2 = np.ones(m) / m
    return A_eq, b_eq, prob_dist, prob_dist_2


def RANSAC_approach(ladder, pattern, LAMBDA, HUN=True, L2=False, iters=None, RN_iters = None, timer=1):
    "algorithm for fitting ladders using RANSAC parameter estimation"
    a = time.time()
    n = len(ladder)
    m = len(pattern)
    A_eq, b_eq, prob_dist, prob_dist_2 = create_matrices_for_LP_and_probs(m,n) # matrices are used in scipy.linprog
    opt_model, cons, x_vars = create_model(m,n)

    pt = np.array(pattern)
    k1, k2 = np.zeros(m + 1), np.zeros(m + 1) # to determine threshold for ransac
    k1[1:], k2[:m] = pt, pt

    J_min, best_coeffs, best_config = np.inf, (0,0), np.ones((1,n)) * (-1)

    inlier_threshold = np.min(np.abs(k1-k2)) * 0.3
    if iters is None:
        q = 11
    else:
        q = iters
    #TEST
    a = time.time()
    for i in range(q):
        coeffs = RANSAC_estimation(m, n, prob_dist, prob_dist_2, pt, ladder, inlier_threshold, RN_iters)
        last_coeffs = coeffs

        for p in range(20):
            if HUN:
                J, cfg = compute_criterion_through_hungarian_extended(ladder, pattern, coeffs, LAMBDA, L2)
            else:
                J, cfg = compute_criterion_for_constant_affine_grb(ladder, pattern, coeffs, opt_model, x_vars, LAMBDA, L2)
            if L2:
                J2, coeffs2 = L2_correction(ladder, cfg, pattern, LAMBDA)
            else:
                J2, coeffs2 = compute_criterion_for_constant_assignment_grb(ladder, cfg, pattern, LAMBDA)
            coeffs = coeffs2
            J = J2
            if coeffs[1] == last_coeffs[1] or p == 19:
                break
            else:
                last_coeffs = coeffs
        if J < J_min:
            J_min, best_coeffs, best_config = J, coeffs, cfg
    if iters is None:
        return J_min, [best_coeffs, best_config]
    return J_min, [best_coeffs, best_config], time.time() - a


def recover_config(config, m, n):
    "support function used to recover assignment from soliton of LP solver"
    cfg = list()
    for i in range(n):
        pushed = False
        for j in range(m):
            if config[m*i + j] > 0:
                cfg.append(j)
                pushed = True
                break
        if not pushed:
            cfg.append(-1)
    return cfg


def compute_criterion_for_constant_assignment(data, assignment, pattern, LAMBDA):
    "finds best coefficients of affine transform, when L1 norm is used, using scipy linprog"
    n = len(assignment)
    c = np.ones(2+n)
    c[0], c[1] = 0, 0
    A_ub = np.zeros((2*n, n+2))
    b_ub = np.zeros(2*n)
    bounds = [(None, None), (0, None)]
    unused = 0
    for i in range(n):
        bounds.append((0, None))
        index = int(2 * i)
        if assignment[i] > -1:
            A_ub[index,0], A_ub[index, 1], A_ub[index, 2 + i] = 1, pattern[int(assignment[i])], -1
            A_ub[index+1, 0], A_ub[index+1, 1], A_ub[index+1, 2 + i] = -1, -pattern[int(assignment[i])], -1
            b_ub[index], b_ub[index+1] = data[i], -data[i]
        else:
            A_ub[index,2+i], A_ub[index+1,2+i], b_ub[index], b_ub[index+1] = 1, -1, 1, -1
            c[2+i] = 0
            unused += 1
    res = opt.linprog(c, A_ub=A_ub, b_ub=b_ub, options={'tol': 1e-8}, bounds=bounds)
    x, fun = res.x, res.fun
    fun += LAMBDA * unused
    x = np.array(x.astype(float))
    return fun, (x[0], x[1])


def generate_ladder_from_pattern(pattern, max_FP=3, p_FN=0.85, p_FP=0.65, sigma=1.):
    "function used to generate synthetic ladders"
    alpha = np.random.randint(-30, 80)
    beta = np.random.rand() * 2.5 + 0.5
    ladder = []
    ldd = []
    p = pattern
    missed = 0
    more = 0
    for j in range(len(p)):
        if np.random.rand() > p_FN:
            missed += 1
            continue
        g = np.random.randn() * sigma**2
        new = alpha + beta * p[j] + beta * g
        ladder.append((new, j))
        ldd.append(new)
    for j in range(max_FP):
        if np.random.rand() > p_FP:
            continue
        more += 1
        new = np.random.rand() * (max(ldd) + 50 - (min(ldd) - 50)) + min(ldd) - 50
        ladder.append((new,-1))
    ladder.sort()
    ladder = np.array(ladder)
    return ladder[:,0], ladder[:,1], alpha, beta


def train_on_synthetic(HUN, L2):
    "function used to train LAMBDA parameters of algorithms"
    tested_values = [5,10,30,50,100,200,500, 1000,5000,10000]
    ss = ["testRR1.txt", "testRR2.txt", "testLR1.txt", "testLR2.txt"]
    its = 80
    sigmas = [0.5,1,1.5]
    algors = [RANSAC_approach,LoRANSAC_approach]
    algors = algors[::-1]
    mode = [False,True]
    mems = []
    for i in range(4):
        mems.append(np.zeros((its, len(tested_values)*len(sigmas))))
    u = 0
    for s in algors:
        for t in mode:
            print("mode ",t)
            g = 0
            for q in sigmas:
                print("sigma ",q)
                for i in range(its):
                    print("ITER: ", i)
                    pattern = PATTERNS[np.random.randint(0, len(PATTERNS))]
                    ladder, cfg, alpha, beta = generate_ladder_from_pattern(pattern, max_FP=6, p_FN=0.90, sigma=q)
                    exp_non = np.sum(cfg == -1)
                    for j, val in enumerate(tested_values):
                        J, k = s(ladder, pattern, val, HUN=False, L2=t)
                        p = abs(k[0][1] - beta) / beta
                        mems[u][i,j + g*len(tested_values)] = p
                g += 1
            u +=1
    print("VALUE:\tMED:\tAVG:\tMAX:")
    vals = [[],[],[]]
    for u in range(4):
        f = open(ss[u],"w")
        for k in range(len(tested_values)):
            f.write(str(tested_values[k]))
            for i in range(len(sigmas)):
                s = " {0} {1}".format(np.median(mems[u][:,len(tested_values) * i + k]), np.average(mems[u][:,i*len(tested_values)+ k]))
                f.write(s)
            f.write("\n")
        f.close()
    c = np.array(mems)


def prepare_ladders(ladders, max_m):
    "support function, sorts and prepares differentiated ladders to be used in fitting algorithms"
    xs = np.zeros(len(ladders))
    k = list()
    max_len, index = 0, -1
    for i, ladder in enumerate(ladders):
        pl = np.zeros(len(ladder)) #-2 for hungarian
        ladder.sort(key=lambda x: x[1])
        step = (len(ladder) - pl.shape[0]) // 2
        for j in range(pl.shape[0]):
            pl[j] = ladder[j + step][1]
            xs[i] += ladder[j+step][0]
        xs[i] /= (pl.shape[0])
        while pl.shape[0] > max_m:
            if abs(pl[0] - pl[1]) > abs(pl[-1] - pl[-2]):
                pl = pl[1:]
            else:
                pl = pl[:pl.shape[0]-1]
        k.append(pl.tolist()) # no list
        if len(k[-1]) > max_len:
            max_len, index = len(k[-1]), i
    if index != 0:
        xs[0], xs[index] = xs[index], xs[0]
        k[0], k[index] = k[index], k[0]
    return xs, k


def create_ladder_model(pattern_values):
    "creates ladder model directly from base pairs values"
    c = -600
    y0, yn = 120, 520
    z0, zn = max(pattern_values), min(pattern_values)
    A = np.array([[1, y0-c],[1, yn-c]])
    B = np.array([y0*(z0-c),yn*(zn-c)])
    (a, b) = list(np.linalg.pinv(A)@B)
    pattern = []
    for i in pattern_values:
        pattern.append(a / (i - c) + b)
    pattern.sort()
    return pattern


def choose_ladder_type_and_interpolate(input_ladders, pattern=None, pattern_values=None, mode=0):
    "wrapper function, calls fitting function, after that interpolates missing bands to ladders, returns BandSolver object"
    if pattern_values is None:
        pattern = PATTERNS[1]
        pattern_values = P_VALUES[1]
    if pattern is None:
        pattern = create_ladder_model(pattern_values)
    min_m = len(pattern)
    xs, ladders = prepare_ladders(input_ladders, min_m)
    J_min, best_mem = 0, []
    for i, ladder in enumerate(ladders):
        J, m = LoRANSAC_approach(ladder, pattern, LAMBDA=LAMBDA_LH_1, HUN=True, L2=False)  # using Hungarian alg. with L1
        J_min += J
        best_mem.append(m)
    ys = np.zeros((len(pattern_values), len(ladders))) - 1
    for i, ladder in enumerate(ladders):
        (coeff, config) = best_mem[i]
        config = np.array(config)
        for j in range(ys.shape[0]):
            k = np.argwhere(config==j)
            if len(k) > 0:
                ys[j,i] = ladder[k[0][0]]
            else:
                ys[j, i] = coeff[0] + coeff[1] * pattern[j]
    bs = BandSolver(pattern, pattern_values, xs, ys, mode)
    return bs
