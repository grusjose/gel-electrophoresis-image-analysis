import picture
import scipy
import scipy.signal
import scipy.ndimage
import numpy as np
import time
import PIL.Image as Image
import os.path as osp
import evaluation
import os
import matplotlib.pyplot as plt
import detection_evaluation

NAIVE_THRESHOLD = 0.1


def return_filter_pattern(path, filename, scale=1):
    "load pattern"
    pattern = picture.Picture(0, 0, path, filename)
    if scale != 1:
        pattern = pattern.avg_pool(scale)
    pattern = pattern.pixels
    return pattern


def preprocess_data(image, pattern):
    "perform data preprocessing"
    image = image.astype("float64")
    image = (image - np.average(image)) / np.sqrt(np.var(image)+0.1)
    pattern = (pattern - np.average(pattern)) / np.sqrt(np.var(pattern)+0.1)
    return image, pattern


def cross_correlate(image, pattern, weight=0.5):
    "correlate input and pattern"
    new_picture = picture.Picture(image.width, image.height)
    array = image.pixels.copy()
    array, pattern = preprocess_data(array, pattern.copy())
    pattern *= weight
    new_picture.pixels = np.clip(scipy.signal.correlate2d(array, pattern, mode="same"), 0, 10000)
    return new_picture


def change_size_of_pattern(pattern, factor):
    "change size of input patter"
    w = int(pattern.shape[1] * factor)
    h = int(pattern.shape[0] * factor)
    new_pattern = np.zeros((h, w))
    used_pixels = np.pad(pattern, [(2,2),(2,2)], mode="edge")
    for y in range(h):
        for x in range(w):
            l = round(x / factor) + 1
            k = round(y / factor) + 1
            a = x / factor - l + 1
            b = y / factor - k + 1
            new_pattern[y, x] = (1 - a) * (1 - b) * used_pixels[k, l] + a * (1 - b) * used_pixels[k, l + 1] + \
                                b * (1 - a) * used_pixels[k + 1, l] + a * b * used_pixels[k + 1, l + 1]
    return new_pattern


def find_local_maxima(image, threshold):
    "found local maximas in correlation output"
    max_data = list()
    max_mem = np.zeros((image.height, image.width)).astype(bool)
    data = scipy.ndimage.maximum_filter(image.pixels,(1,3)).astype(int)
    data = ((data > threshold) * data).astype(int)
    while True:
        current_value = np.max(data)
        if current_value < threshold:
            break
        indices = np.argwhere(data==current_value)
        for index in indices:
            if data[index[0],index[1]] != 0:
                max_data.append(flood_fill(data, index[1], index[0]))
                if min(max_data[-1][1], image.height - max_data[-1][1]) < 0.03 * image.height:
                    max_data.pop()
            else:
                pass
    for x,y in max_data:
        max_mem[y,x] = True
    return max_data, scipy.ndimage.binary_dilation(max_mem, np.ones((3,3)),1)


def flood_fill(array, start_x, start_y):
    "support BFS for local maxima localisation"
    center_x, center_y, number = 0, 0, 0
    value = array[start_y, start_x]
    memory = [(start_x, start_y, value)]
    array[start_y,start_x] = 0
    mem = np.zeros(array.shape)
    while len(memory) > 0:
        x, y, val = memory.pop()
        mem[y,x] = 255
        if val > 0.95*value:
            center_x += x
            center_y += y
            number +=1
        for i in range(-1,2):
            for j in range(-1,2):
                new_x = x + j
                new_y = y + i
                if picture.is_in_image(new_x, new_y, array.shape):
                    new_val = array[new_y, new_x]
                    if new_val != 0 and 0.97*new_val <= val:
                        array[new_y, new_x] = 0
                        memory.append((new_x, new_y, new_val))
    return int(center_x / number), int(center_y / number)


def training_sequence(path, filename, tested_values):
    "sequence to find best threshold"
    pattern_u = return_filter_pattern(path, "pattern.jpg", 1)
    pattern_u = change_size_of_pattern(pattern_u, 0.95)
    image = picture.Picture(0, 0, path, filename)
    i = image.avg_pool(2)

    new = cross_correlate(i, pattern_u)
    s = filename.find(".")
    saved = path + filename[:s] + ".npy"
    if osp.exists(saved):
        saved_extremes = np.load(saved).astype(int)
    else:
        saved_extremes = None
    if saved_extremes is None:
        print("missing annotation")
        return None
    accuracies = []
    for j in tested_values:
        extremes, _ = detection_evaluation.locate_local_maxima(new, j)
        new_ex = []
        for extreme in extremes:
            new_ex.append([extreme[0] * i.pool_factor, extreme[1] * i.pool_factor])
        extremes = new_ex
        _, acc, _ = picture.accuracy_computation(extremes, saved_extremes)
        accuracies.append(acc)
    return accuracies


def detection_sequence(path, filename, save=False, special=False):
    "sequence of detection and evalutation operations"
    pattern_u = return_filter_pattern(path, "pattern.jpg", 1)
    pattern_u = change_size_of_pattern(pattern_u, 0.95)
    image = picture.Picture(0, 0, path, filename)
    i = image.avg_pool(2)

    new = cross_correlate(i, pattern_u)
    extremes, _ = detection_evaluation.locate_local_maxima(new, NAIVE_THRESHOLD)
    new_ex = []
    for extreme in extremes:
        new_ex.append([extreme[0] * i.pool_factor, extreme[1] * i.pool_factor])
    extremes = new_ex
    accuracy = 0
    rs = None
    s = filename.find(".")
    saved = path + filename[:s] + ".npy"
    if osp.exists(saved):
        saved_extremes = np.load(saved).astype(int)
        accuracy, _, selected = picture.accuracy_computation(extremes.copy(), saved_extremes)
        #image.create_rgb_with_marks(selected, show=True)
        if special:
            rs = []
            for i in [5,10, 15, 20, 25, 30, 40, 50, 80,120,150]:
                _, rsi, _ = picture.accuracy_computation(extremes.copy(), saved_extremes, width=i)
                rs.append(rsi)
    else:
        selected = [extremes,[],[],[]]
        accuracy = None

    return accuracy, rs

