from PIL import Image
import numpy as np
import os
import os.path as osp
import picture
from scipy.fftpack import ifft2, fft2, ifftshift
import detection_evaluation
import time
import sys

TILE_WIDTH = 256
TILE_HEIGHT = 128

NORMAL_THRESHOLD = 0.45
WIDE_THRESHOLD = 0.4
WIDE_SAMPLES = [102,101,17,23,26,31,34,37,53,62,64,65,71,75,76,103,105]
NEG_list = ["ob0.","ob2.","ob7.", "ob11.","ob14.", "ob17.","ob18."]
NEG_vals = [[(838,465)],[(1190,253)],[(489,218),(490,283)],[(299,239),(370,225),(520,190),(518,279)],
            [(351,281),(209,323)],[(257,235),(259,331),(381,299),(519,291),(1347,413),(1337,461), (1359,410),(1387,474)],
            [(183,293),(174,320),(176,369),(353,322),(458,458),(794,289),(353,307),(795,357),(977,300)]]


def edge_situation(x, y, pixels, marks):
    "if subwindow is not covered in array, pad array appropriately"
    p = True
    [height, width] = pixels.shape
    expand = [0, 0, 0, 0]
    left_x = max(0, x - TILE_WIDTH // 2)
    expand[2] = -min(0, x - TILE_WIDTH // 2)
    right_x = min(x + TILE_WIDTH // 2, width)
    expand[3] = max(0, x + TILE_WIDTH // 2 - width)
    up_y = max(0, y - TILE_HEIGHT // 2)
    expand[0] = -min(0, y - TILE_HEIGHT // 2)
    down_y = min(height, y + TILE_HEIGHT // 2)
    expand[1] = max(0, y + TILE_HEIGHT // 2 - height)
    for i in range(4):
        if expand[i] != 0:
            p = False
    f = np.pad(pixels[up_y:down_y, left_x:right_x], (expand[0:2], expand[2:4]), "edge").astype("float32")
    g = np.pad(marks[up_y:down_y, left_x:right_x], (expand[0:2], expand[2:4]), "edge").astype("float32")
    return f, g, p


def show_array(array, const, norm=False, save=False, name=""):
    "support function, used to show given array"
    if norm:
        a = (array - np.min(array)) / (np.max(array) - np.min(array)) * 255
    else:
        a = np.clip(np.real(array) * const, 0, 255).astype(int)
    p = Image.fromarray(a.astype("uint8"))
    if save:
        p.save(name)
    else:
        p.show()
    p.close()


def repos_fft(fft):
    "swap quadrants of FFT for visualisation"
    x_s = fft.shape[1] // 2
    y_s = fft.shape[0] // 2
    x1 = fft[0:y_s, 0:x_s]
    x2 = fft[0:y_s, x_s:]
    x3 = fft[y_s:, 0:x_s]
    x4 = fft[y_s:, x_s:]
    y1 = np.concatenate((x2, x1), axis=1)
    y2 = np.concatenate((x4, x3), axis=1)
    y = np.concatenate((y2, y1), axis=0)
    return y


def teach_on_picture(pixels, marks, indices, negs, eps=0.1):
    "iterate over every mark of image, calculate exact filter and other spectrums"
    energy = np.zeros((2 * TILE_HEIGHT, 2 * TILE_WIDTH)).astype("complex128")
    target = np.zeros((2 * TILE_HEIGHT, 2 * TILE_WIDTH)).astype("complex128")
    exacts = np.zeros((2 * TILE_HEIGHT, 2 * TILE_WIDTH)).astype("complex128")
    num_of_exacts = 0

    if not negs is None:  # if negative examples are given, expand list of positions to train on
        indices = np.concatenate((indices, np.array(negs)))
    eps = np.complex(eps)
    for i in range(indices.shape[0]):
        [x, y] = indices[i]
        ff, gg, p = edge_situation(x, y, pixels, marks)
        ff = np.real(ff)
        if not p:
            continue
        ff = np.pad(ff, ((0, 1 * TILE_HEIGHT), (0, 1 * TILE_WIDTH)), mode="constant")
        gg = np.pad(gg, ((0, 1 * TILE_HEIGHT), (0, 1 * TILE_WIDTH)), mode="constant")
        F = fft2(ff)
        G = fft2(gg)
        exacts += G * np.conj(F) / (F * np.conj(F) + eps)
        target += G * np.conj(F)
        energy += F * np.conj(F)
        num_of_exacts += 1
    return [target, energy, exacts, num_of_exacts]


def teach_on_set(number, path, eps, wide=False):
    "teach correlation filters on given set of images"
    energy_mem = np.zeros((2 * TILE_HEIGHT, 2 * TILE_WIDTH)).astype("complex128")
    target_mem = np.zeros((2 * TILE_HEIGHT, 2 * TILE_WIDTH)).astype("complex128")
    exacts_mem = np.zeros((2 * TILE_HEIGHT, 2 * TILE_WIDTH)).astype("complex128")
    num_mem = 0
    j = 1
    for filename in os.listdir(path):
        if "ob" in filename and "_" not in filename and ".jpg" in filename:
            is_wide = False
            for i in WIDE_SAMPLES:
                if str(i) in filename:
                    is_wide = True
                    break
            if (wide and not is_wide) or (not wide and is_wide):
                continue
            p = picture.Picture(0, 0, path, filename)
            p.pixels = p.pixels.astype("float32")
            p = p.local_normalization()
            pixels = p.pixels.astype("float32")
            i = filename.find(".")
            sigma = 2.5
            saved_extremes = np.load(path + filename[:i] + ".npy").astype(int) // p.pool_factor
            neg = None
            for i, name in enumerate(NEG_list):
                if name in filename:
                    neg = NEG_vals[i]
                    break
            marks = picture.create_gaussian_markers(saved_extremes, pixels.shape, sigma, negs=neg)
            data = teach_on_picture(pixels, marks, saved_extremes, neg, eps)
            target_mem += data[0]
            energy_mem += data[1]
            exacts_mem += data[2]
            num_mem += data[3]
            j += 1
            if j == number:
                break
    asef_filter = np.conj(exacts_mem / num_mem)  # ASEF filter in Fourier domain
    mosse_filter = np.conj(target_mem / (energy_mem + eps))  # MOSSE filter in Fourier domain
    m = np.real(ifft2(mosse_filter))
    a = np.real(ifft2(asef_filter))
    m = repos_fft(m)[TILE_HEIGHT // 2:3 * TILE_HEIGHT // 2, TILE_WIDTH // 2:3 * TILE_WIDTH // 2]
    a = repos_fft(a)[TILE_HEIGHT // 2:3 * TILE_HEIGHT // 2, TILE_WIDTH // 2:3 * TILE_WIDTH // 2]
    return m, a


def nearest_second_power(x):
    "compute nerarest power of 2 for enlarging windows"
    return 1 << (x - 1).bit_length()


def fft_filter(image, filter, show=False, filter2 = None):
    "generate heatmaps from image and filters"
    double_filtration = not filter2 is None
    filter = np.load(filter)
    if double_filtration:
        filter2 = np.load(filter2)
    image.pixels = image.pixels.astype("float32")
    px, py = max(0,image.width - filter.shape[1]), max(0,image.height - filter.shape[0])
    filter = np.pad(filter, ((py//2,py-py//2),(px//2, px-px//2)),mode="constant")
    if double_filtration:
        filter2 = np.pad(filter2, ((py // 2, py - py // 2), (px // 2, px - px // 2)), mode="constant")
    shape = (nearest_second_power(2*image.height), nearest_second_power(2*image.width))
    new = image.local_normalization()
    F = fft2(new.pixels, shape)
    H = fft2(filter, shape)
    G = F * np.conj(H)
    g = ifft2(G)
    g = np.real(g)
    g = repos_fft(g)
    lim_x, lim_y = g.shape[1]//2 - image.width//2, g.shape[0]//2 - image.height//2
    g = g[lim_y:lim_y+ image.height,lim_x:lim_x + image.width]
    output = picture.Picture(g.shape[1], g.shape[0])
    output.pixels = g
    output.pool_factor = image.pool_factor
    if double_filtration:
        H = fft2(filter2, shape)
        G = F * np.conj(H)
        g = ifft2(G)
        g = np.real(g)
        g = repos_fft(g)
        lim_x, lim_y = g.shape[1] // 2 - image.width // 2, g.shape[0] // 2 - image.height // 2
        g = g[lim_y:lim_y + image.height, lim_x:lim_x + image.width]
        output2 = picture.Picture(g.shape[1], g.shape[0])
        output2.pixels = g
        output2.pool_factor = image.pool_factor
        return output, output2
    if show:
        show_array(output.pixels, 1, True)
    return output


def training_sequence(path, filename, tested_values, wide):
    "sequence used for training threshold"
    image = picture.Picture(0, 0, path, filename)
    if wide:
        is_wide = False
        for i in WIDE_SAMPLES:
            if str(i) in filename:
                is_wide = True
                break
        if not is_wide:
            return None
        correlation_output = fft_filter(image, "mosse_filter_wide.npy", False)
    else:
        correlation_output = fft_filter(image, "mosse_filter.npy", False)
    s = filename.find(".")
    saved = path + filename[:s] + ".npy"
    if osp.exists(saved):
        saved_extremes = np.load(saved).astype(int) // image.pool_factor
    else:
        saved_extremes = None
    if saved_extremes is None:
        return None
    accuracies = []
    for i in tested_values:
        extremes, _ = detection_evaluation.locate_local_maxima(correlation_output, i, w=wide)
        _, acc, _ = picture.accuracy_computation(extremes, saved_extremes)
        accuracies.append(acc)
    return accuracies


def detection_sequence(path, filename, save=False, show=False, special=False):
    "detection sequence and following measurement operations"
    image = picture.Picture(0, 0, path, filename)
    o1, o2 = fft_filter(image, "mosse_filter_wide.npy", False, "mosse_filter.npy")
    s = filename.find(".")
    saved = path + filename[:s] + ".npy"
    o1.path = path
    o2.path = path
    extremes1, r1 = detection_evaluation.locate_local_maxima(o1, WIDE_THRESHOLD, w=True)
    extremes2, r2 = detection_evaluation.locate_local_maxima(o2, NORMAL_THRESHOLD)
    if r2 < r1:  # decide type of filter to be used
        extremes = extremes2
    else:
        extremes = extremes1
    if osp.exists(saved):
        saved_extremes = np.load(saved).astype(int) // image.pool_factor
    else:
        saved_extremes = None
    text = ["Nalezené reference", "FP detekce", "TP detekce", "Nenalezené anotace"]
    rs = None
    if osp.exists(saved):
        accuracy, g, selected = picture.accuracy_computation(extremes.copy(), saved_extremes)
        image.create_rgb_with_marks(selected, save=save, show=show, path=(path + "cc_det\\"), text=text)
        if special:
            rs = []
            for i in [5,10, 15, 20, 25, 30, 40, 50, 80,120,150]:
                _, rsi, _ = picture.accuracy_computation(extremes.copy(), saved_extremes, width=i)
                rs.append(rsi)
    else:
        selected = [extremes, []]
        accuracy = None
    im, lad, po, lines, angle = detection_evaluation.locate_ladders_and_lines(extremes, image)
    k = []
    for i in lad:
        k += i
    l = []
    for i in po:
        l += i
    text = ["Detekce značek žebříčku", "Detekce neznámých značek"]
    im.create_rgb_with_marks([k, l, [], []], save=save, show=show, path=(path + "cc_det\\"), subtype="_fin", text=text)
    im.visualise_lines(lines, save=save, path=(path + "cc_det\\"))
    # generate fitting for all training ladders

    import evaluation

    for j in [True]:
        for i in range(3):
            bs = evaluation.choose_ladder_type_and_interpolate(lad, evaluation.PATTERNS[i], evaluation.P_VALUES[i])
            if j:
                s = "true_"
            else:
                s = "false_"
            im.final_visualisation(bs, po, save=save, show=show, path=(path + "cc_det\\"),
                                   subtype=("vystup_index_" + s + str(i)), text=text)
    return accuracy, rs


def automatic_ladder_generation(path, filename, nm):
    "generate prediction of model, operator edits prediction by mouse later"
    image = picture.Picture(0, 0, path, filename)
    o1, o2 = fft_filter(image, "mosse_filter_wide.npy", False, "mosse_filter.npy")
    extremes1, r1 = detection_evaluation.locate_local_maxima(o1, WIDE_THRESHOLD)
    extremes2, r2 = detection_evaluation.locate_local_maxima(o2, NORMAL_THRESHOLD)
    if r2 < r1:
        extremes = extremes2
    else:
        extremes = extremes1
    new_image, ladders, _, _, _ = detection_evaluation.locate_ladders_and_lines(extremes, image)

    ladder = None
    max_l = 0
    for l in ladders:
        if len(l) > max_l:
            max_l, ladder = len(l), l
    if not ladder is None and len(ladder) > nm//2:
        x = 0
        top, bottom = image.height, 0
        for i in ladder:
            if i[1] < top:
                top = i[1]
            if i[1] > bottom:
                bottom = i[1]
            x += i[0]
        x = int(x / len(ladder))
        top, bottom = int(top/2), int(min(image.height-1, 2 * (image.height-1 + bottom) / 3))
        left, right = max(x - 80,0), min(x + 80, new_image.width-1)
        image = Image.fromarray(new_image.pixels[top:bottom,left:right])
        while len(ladder) > nm:
            ladder.pop()
        data = []
        for i in ladder:
            data.append((i[0] - left, i[1] - top))
    else:
        data, image = [], Image.fromarray(new_image.pixels)
    return data, image


def detection_app(path, filename, ladder, mode, ev_mode, reusable_data, tm):
    "detection sequence and following measurement operations"
    image = picture.Picture(0, 0, path, filename)
    text = ["Detekce značek žebříčku", "Detekce neznámých značek"]
    if reusable_data is None:
        o1, o2 = fft_filter(image, "mosse_filter_wide.npy", False, "mosse_filter.npy")
    else:
        o1, o2 = reusable_data[0], reusable_data[1]
    extremes1, r1 = detection_evaluation.locate_local_maxima(o1, WIDE_THRESHOLD * tm / 100., w=True)
    extremes2, r2 = detection_evaluation.locate_local_maxima(o2, NORMAL_THRESHOLD * tm / 100.)

    if ev_mode == "GRB":
        import evaluation
    else:
        import evaluation_nogrb as evaluation

    if r2 < r1:
        extremes = extremes2
    else:
        extremes = extremes1
    im, lad, po, lines, _ = detection_evaluation.locate_ladders_and_lines(extremes, image)
    if ladder[0] is None:
        bs = evaluation.choose_ladder_type_and_interpolate(lad, None, None, mode)
    else:
        ladder[0].sort()
        pattern_vals = ladder[0][::-1]
        if ladder[1] is None:
            pass
        else:
            ladder[1].sort()
        pattern = ladder[1]
        bs = evaluation.choose_ladder_type_and_interpolate(lad, pattern, pattern_vals, mode)
    return im.final_visualisation(bs, po, text=text), [o1, o2]