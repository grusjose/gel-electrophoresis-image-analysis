import picture
import numpy as np
import scipy.ndimage
from PIL import Image
import cv2
import os
import os.path as osp
import detection_evaluation
import sys

cascade = cv2.CascadeClassifier("cascade_lbp1.xml")  # load trained detector

DETECTION_WIDTH = 66
DETECTION_HEIGHT = 26

POINT_THRESHOLD = 11


def edge_situation(x, y, pixels, off=False):
    "pad part of the image, if detection window size conditions are not met"
    if off:
        [height, width] = pixels.shape
        left_x = x - DETECTION_WIDTH//2 - 40
        right_x = x + DETECTION_WIDTH // 2 + 40
        up_y = y - DETECTION_HEIGHT // 2 - 20
        down_y = y + DETECTION_HEIGHT // 2 + 20
    else:
        [height, width] = pixels.shape
        left_x = x - DETECTION_WIDTH//2
        right_x = x + DETECTION_WIDTH // 2
        up_y = y - DETECTION_HEIGHT // 2
        down_y = y + DETECTION_HEIGHT // 2
    padding = [[0,0],[0,0]]
    if left_x < 0:
        padding[1][0] = -left_x
        left_x = 0
    elif right_x >= width:
        off = right_x - width + 1
        right_x = width - 1
        padding[1][1] = off
    if up_y < 0:
        padding[0][0] = -up_y
        up_y = 0
    elif down_y >= height:
        off = down_y - height + 1
        down_y = height - 1
        padding[0][1] = off
    f = np.pad(pixels[up_y:down_y, left_x:right_x], padding, "edge")
    return f


def annotation(path):
    "create annotation for subwindows"
    real = open(path + "real.dat", "w")
    for filename in os.listdir(path + "real/"):
        name = os.path.join(path + "real/", filename)
        real.write(name)
        real.write(" 1 ")
        real.write("{} {} {} {}".format(3, 4, DETECTION_WIDTH-6, DETECTION_HEIGHT-8))
        real.write("\n")
    real.close()


def create_dataset_for_subwindow(path, name, saved_extremes, scale, index, real, fake):
    "create samples for cascade classifier training"
    print(path, name)
    if "obn" in name or "19" in name:
        return
    pic = picture.Picture(0,0, path, name)
    pixels = (pic.avg_pool(scale)).pixels
    extremes = (np.load(saved_extremes) / scale).astype(int)
    labels = np.zeros((extremes.shape[0]*3,1))
    for i in range(extremes.shape[0]):
        x, y = extremes[i, 0], extremes[i, 1]
        a = edge_situation(x, y, pixels)
        a = Image.fromarray(a.astype("uint8"))
        filename = path + "real/" + str(index) + "_" + str(i)+ ".png"
        a.save(filename)
        a.close()
        real.write(filename)
        real.write(" 1 ")
        real.write("{} {} {} {}".format(DETECTION_WIDTH//2 - 32, DETECTION_HEIGHT//2 - 6, 64, 12))
        real.write("\n")
    for i in range(4*extremes.shape[0]):
        x,y = -1, -1
        while True:
            out_of_bounds = True
            x = np.random.randint(pixels.shape[1])
            y = np.random.randint(pixels.shape[0])
            for j in range(extremes.shape[0]):
                if max(abs(x-extremes[j,0]),abs(y-extremes[j,1])) < 65:
                    out_of_bounds = False
                    break
            if out_of_bounds:
                break
        a = edge_situation(x, y, pixels, off=True)
        filename = path + "fake/" + str(index) + "_" + str(i) + ".png"
        a = Image.fromarray(a.astype("uint8"))
        a.save(filename)
        a.close()
        fake.write(filename + "\n")


def process_binary_array(b_array, scale, threshold):
    "perform watershed segmentation, return extremes from groups consisting of more than threshold pixels"
    array = scipy.ndimage.distance_transform_edt(b_array)
    extremes = []
    while True:
        val = np.amax(array)
        if val <= 0:
            break
        result = np.argwhere(array >= val)
        for y, x in result:
            agg, avg_x, avg_y = flood_fill(array, x, y)
            avg_x, avg_y = scale * (avg_x-20), scale*(avg_y-20)
            if agg < threshold:
                continue
            to_add = True
            for i, extreme in enumerate(extremes):
                if abs(extreme[0] - avg_x) < 60 and abs(extreme[1] - avg_y) < 10:
                    extremes[i] = (int((extreme[0] + avg_x) // 2), int((extreme[1] + avg_y) // 2))
                    to_add = False
                    break
            if to_add:
                extremes.append((int(avg_x), int(avg_y)))
    return extremes


def flood_fill(array, start_x, start_y):
    "support BFS for local maxima localisation"
    value = array[start_y, start_x]
    memory = [(start_x, start_y, value)]
    array[start_y,start_x] = 0
    agg = 0
    agg_x, agg_y = 0, 0
    while len(memory) > 0:
        x, y, val = memory.pop()
        agg_x += x
        agg_y += y
        agg += 1
        for i in range(-1,2):
            for j in range(-1,2):
                new_x = x + j
                new_y = y + i
                if picture.is_in_image(new_x, new_y, array.shape):
                    new_val = array[new_y, new_x]
                    if 0 < new_val <= val:
                        array[new_y, new_x] = 0
                        memory.append((new_x, new_y, new_val))
    return agg, agg_x / agg, agg_y / agg


def detect_objects(image, scale, ext=None):
    "image classification by trained cascade classifier using detect multiscale, then perform morphology operations"
    pic = image.avg_pool(scale)
    px = pic.pixels.astype("uint8")
    px = np.pad(px, ((20,20),(20,20)),mode="constant")
    output = np.zeros(px.shape)
    for j in range(1):
        out = cascade.detectMultiScale(px, minNeighbors=0, maxSize=(400, 20), minSize=(10,0))
        for (x,y,w,h) in out:
            output[y+h//2,x+w//2] = 1
            cv2.rectangle(px, (x+w//2,y+h//2),(x+w//2,y+h//2),(255,255,255),2)
    output = scipy.ndimage.binary_dilation(output, structure=np.array([[1,1,1,1,1]]))
    output = scipy.ndimage.binary_closing(output, iterations=1, structure=np.array([[1,1],[1,1]])) #1
    return output


def training_sequence(path, filename, tested_values):
    "sequence used to find optimal threshold threshold"
    image = picture.Picture(0, 0, path, filename)
    scale = 2
    s = filename.find(".")
    saved = path + filename[:s] + ".npy"
    if osp.exists(saved):
        saved_extremes = np.load(saved).astype(int) // image.pool_factor
    else:
        saved_extremes = None
    if saved_extremes is None:
        print("missing annotation")
        return None
    accuracies = []
    b_array = detect_objects(image, scale)
    for i in tested_values:
        print("threshold", i)
        extremes = process_binary_array(b_array, scale, i)
        _, acc, _ = picture.accuracy_computation(extremes, saved_extremes)
        accuracies.append(acc)
    return accuracies


def detection_sequence(path, filename, save=False, show=False, special=False):
    "detection sequence and following measurement operations"
    image = picture.Picture(0, 0, path, filename)
    scale = 2
    s = filename.find(".")
    saved = path + filename[:s] + ".npy"
    if osp.exists(saved):
        saved_extremes = np.load(saved).astype(int) // image.pool_factor
    else:
        saved_extremes = None
    b_array = detect_objects(image, scale, ext=saved_extremes)
    extremes = process_binary_array(b_array, scale, POINT_THRESHOLD)
    text = ["Nalezené reference", "FP detekce", "TP detekce", "Nenalezené anotace"]
    rs = None
    if osp.exists(saved):
        accuracy, g, selected = picture.accuracy_computation(extremes.copy(), saved_extremes)
        image.create_rgb_with_marks(selected, save=save, show=show, path=(path + "cc_det2\\"), text=text)
        if special:
            rs = []
            for i in [5, 10, 15, 20, 25, 30, 40, 50, 80, 120, 150]:
                _, rsi, _ = picture.accuracy_computation(extremes.copy(), saved_extremes, width=i)
                rs.append(rsi)
    else:
        selected = [saved_extremes, extremes]
        accuracy = None
    im, lad, po, lines, _ = detection_evaluation.locate_ladders_and_lines(extremes, image)
    k = []
    for i in lad:
        k += i
    l = []
    for i in po:
        l += i
    text = ["Detekce značek žebříčku", "Detekce neznámých značek"]
    p = im.create_rgb_with_marks([k, l, [], []], save=save, show=show, path=(path + "cc_det2\\"), subtype="_fin", text=text)
    im.visualise_lines(lines, save=save, path=(path + "cc_det2\\"))
    # generate fitting for all reference ladders

    import evaluation

    for j in [True]:
        for i in range(3):
            bs = evaluation.choose_ladder_type_and_interpolate(lad, evaluation.PATTERNS[i], evaluation.P_VALUES[i], mode=0)
            if j:
                s = "true_"
            else:
                s = "false_"
            im.final_visualisation(bs, po, save=save, show=show, path=(path + "cc_det2\\"), subtype=("vystup_index_" + s + str(i)), text=text)
    return accuracy, rs


def detection_app(path, filename, ladder, mode, ev_mode, reusable_data, tm):
    "function called from application"
    image = picture.Picture(0, 0, path, filename)
    scale = 2
    if reusable_data is None:
        b_array = detect_objects(image, scale)
    else:
        b_array = reusable_data
    extremes = process_binary_array(b_array, scale, POINT_THRESHOLD * tm / 100.)
    im, lad, po, lines, _ = detection_evaluation.locate_ladders_and_lines(extremes, image)

    if ev_mode == "GRB":
        import evaluation
    else:
        import evaluation_nogrb as evaluation

    text = ["Detekce značek žebříčku", "Detekce neznámých značek"]
    if ladder[0] is None:
        bs = evaluation.choose_ladder_type_and_interpolate(lad, None, None, mode)
    else:
        ladder[0].sort()
        pattern_vals = ladder[0][::-1]
        if ladder[1] is None:
            pass
        else:
            ladder[1].sort()
        pattern = ladder[1]
        bs = evaluation.choose_ladder_type_and_interpolate(lad, pattern, pattern_vals, mode)
    return im.final_visualisation(bs, po, text=text), b_array