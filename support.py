#!/usr/bin/env python3

import picture
import naive_detection
import os
import numpy as np
import mosse
import cascade_filter
import matplotlib.pyplot as plt
import sys


def generate_pictures(path, dir_name):
    "support function, creates arrays of mark positions from forwarded hand annotated images"
    d = os.path.join(path, dir_name)
    if not os.path.exists(d):
        os.mkdir(d)
    for filename in os.listdir(path):
        if "_m" in filename and  ".png" in filename.lower():

            i = filename.find("_")
            nppath = path + filename[:i] + ".npy"
            if os.path.isfile(nppath):
                continue
            x = picture.create_label_images(path+filename)
            np.save(path + filename[:i], np.array(x))
            print(filename)
            continue
        if "." in filename:
            continue


def generate_detection(path):
    "detect and evaluate all labeled data and save accuracies"
    #txt = open("acc_n.txt","w")
    #rss = open("rs_n.txt","w")
    p = [5, 10, 15, 20, 25, 30, 40, 50, 80,120,150]
    tps = np.zeros((len(p), 3))
    k = True
    num = 0
    for filename in os.listdir(path):
        filename = filename.lower()
        if ("_" not in filename and ".jpg" in filename.lower() and "ob" in filename): #or "obn" in filename:
            if "55" in filename:
                k = True
            if not k:
                continue
            #acc, rs = naive_detection.detection_sequence(path, filename, save=True, special=True)
            print(filename)
            #acc, rs = mosse.detection_sequence(path, filename, save=True, special=True)
            acc, rs = cascade_filter.detection_sequence(path, filename, save=True, special=True)
            print(acc)
            if acc is None:
                continue
            text = filename + " " + str(acc) + "\n"
            #txt.write(text)
            if rs is None:
                continue
            for i in range(len(rs)):
                tps[i,:] += np.array(rs[i])
            num+=1
    for i in range(len(p)):
        s = str(p[i]) + " " + str(tps[i,0] / (tps[i,0] + tps[i,2]) + 0.01) + " " + str(tps[i,0] / (tps[i,0] + tps[i,1]) + 0.01) +"\n"
        #rss.write(s)
    #rss.close()
    #txt.close()


def generate_viola_data(path, lim):
    "cut out samples from images for cascade classifier training"
    fake = open(path +"fake.txt", "w")
    real = open(path + "real.dat", "w")
    i = 0
    for filename in os.listdir(path):
        f = filename.lower()
        if "_" not in f and ".jpg" in f and "ob" in f:
            print(path + filename)
            s = filename.find(".")
            saved_extremes = path + filename[:s] + ".npy"
            cascade_filter.create_dataset_for_subwindow(path, filename, saved_extremes,2, i, real, fake)
            i+=1
            if i == lim:
                break
    fake.close()
    real.close()


def optimal_weight(nm, path, wide):
    "compute optimal weight of threshold by iteration"
    acc_array = []
    tested_values = list(np.arange(4,10,1) / 1)
    tested_values = [0.03,0.05,0.1,0.15,0.2,0.25]
    f = open("opt_n.txt","w")
    i=0
    TT = [[0,0,0] for i in range(len(tested_values))]
    for filename in os.listdir(path):
        if "_" not in filename and ".jpg" in filename.lower():
            print(filename)
            #data = cascade_filter.training_sequence(path, filename, tested_values)
            #data = mosse.training_sequence(path, filename,tested_values, wide)
            data = naive_detection.training_sequence(path, filename, tested_values)
            if not data is None:
                for j in range(len(tested_values)):
                    TT[j][0] += data[j][0]
                    TT[j][1] += data[j][1]
                    TT[j][2] += data[j][2]
                i+=1
            print(i)
            if i == nm:
                break
    data = []
    max_index, max_v = -1, 10
    for i, tested in enumerate(tested_values):
        prec, rec = TT[i][0] / (0.1 + TT[i][0] + TT[i][1]), TT[i][0] / (0.1 + TT[i][0] + TT[i][2])
        s = str(rec) + " " + str(prec) + "\n"
        f.write(s)
        data.append((rec, prec))
        v = (1 - prec)**2 + (1 - rec)**2
        if v < max_v:
            max_index, max_v = i, v
    print("best for ", tested_values[max_index])
    f.close()
    return data


def visualize_rec_and_prec(path, filenames):
    labels = ["MOSSE - wide", "MOSSE - normal", "Kaskádní detektor", "Naivní filtr"]
    colors = ["red", "green","blue","black"]
    k = []
    print("ti")
    for filename in filenames:
        f = open(path + filename, "r")
        duos = []
        print(filename)
        for line in f:
            data = line.split()
            duos.append(list(map(float, data)))
            print(duos[-1])
        k.append(np.array(duos))
        f.close()
    for i in range(len(k)):
        w = k[i]
        plt.plot(w[:,0], w[:,1], color=colors[i], label=labels[i])
    plt.grid()
    plt.xlabel("Recall")
    plt.ylabel('Precision')
    plt.title('Křivky recall / precision')
    plt.legend()
    plt.ylim(0,1)
    plt.xlim(0,1)
    plt.show()


def visualize_multiple_rec_on_tresh(path, filenames):
    labels = ["MOSSE - recall", "MOSSE - precision", "ML - recall", "ML - precision","naivní filtr - recall","naivní filtr - precision"]
    colors = ["red", "blue","green"]
    type = ["solid", "dashed"]
    p = [5,10, 15, 20, 25, 30, 40, 50, 80,120,150]
    k = []
    for filename in filenames:
        f = open(path + filename, "r")
        k1, k2 = [], []
        for line in f:
            data = line.split()
            k1.append(float(data[1]))
            k2.append(float(data[2]))
        k.append(k1)
        k.append(k2)
        f.close()
    for i in range(len(k)):
        w = k[i]
        print(p)
        plt.plot(np.array(p), np.array(w), color=colors[i//2], label=labels[i], linestyle=type[i%2])
    #plt.legend(eps)
    plt.grid()
    plt.xlabel("Geometrický práh")
    plt.ylabel('-')
    plt.title('Závislost recall a precision na změně geometrického prahu')
    plt.legend(loc=4)
    plt.show()


def create_histogram(path, filenames):
    "create histogram from computed accuracies"
    x = np.arange(0, 11) / 10
    x1 = np.arange(0,11)
    k = []
    for filename in filenames:
        f = open(path + filename, "r")
        numbers = [0 for i in range(11)]
        for line in f:
            data = line.split()
            n = round(10* float(data[1]))
            numbers[n] += 1
        numbers = np.array(numbers) / sum(numbers)
        k.append(numbers)
        print(numbers)
        f.close()
    print(len(k))
    w = 0.3
    plt.bar(x1 - w, np.array(k[0]), width=w, color='r', align='center')
    plt.bar(x1, np.array(k[1]), width=w, color='g', align='center')
    plt.bar(x1 + w, np.array(k[2]), width=w, color='b', align='center')
    plt.autoscale()
    plt.grid()
    plt.xticks(x1, x)
    plt.xlabel("Přesnost")
    plt.ylabel('Relativní výskyt')
    plt.title('Výsledky detekcí na olabelované množině obrázků')
    colors = {'Naivní filtr': 'red', 'MOSSE filtr': 'green', 'Kaskádní detektor': 'blue'}
    labels = list(colors.keys())
    handles = [plt.Rectangle((0, 0), 1, 1, color=colors[label]) for label in labels]
    plt.legend(handles, labels)
    plt.show()


def train_eps_and_weight(path, wide=True):
    "training for mosse filter"
    rocs = []
    eps = [0.001]#[0.001,0.01]#, 0.1]#, 0.1]#,0.01]
    for e in eps:
        #mo, a = mosse.teach_on_set(65, path, e, wide=wide)
        #if wide:
        #    np.save("mosse_filter_wide.npy", mo)
        #    np.save("asef_filter_wide.npy", a)
        #else:
        #    np.save("mosse_filter.npy", mo)
        #    np.save("asef_filter.npy", a)
        n = optimal_weight(50, path, wide)
        rocs.append(n)
    for i in range(len(eps)):
        r = np.array(rocs[i])
        print(r.shape)
        print(r)
        plt.plot(r[:,0], r[:,1])
    plt.legend(eps)
    plt.grid()
    plt.xlabel("Recall")
    plt.ylabel('Precision')
    plt.title('Křivka recall / precision')
    plt.ylim(0,1)
    plt.xlim(0,1)
    plt.show()


def recreate_tags(path):
    "tags recreation for cascade classifier training"
    real = open(path + "real.dat", "w")
    path = path + "real/"
    for filename in os.listdir(path):
        print(filename)
        real.write(path+filename)
        real.write(" 1 ")
        real.write("{} {} {} {}".format(64//2 - 26, 20//2 - 8, 52, 16))
        real.write("\n")
    real.close()


def main():
    "sequences used for training and testing"
    path = ""
    im = ""


if __name__ == '__main__':
    main()
