from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import numpy as np
import scipy.signal
import scipy.ndimage
import os
import scipy.misc

TILE_WIDTH = 512
TILE_HEIGHT = 128


class Picture:
    def __init__(self, width, height, path="", name=""):
        self.width = width
        self.height = height
        self.pixels = None
        self.pool_factor = 1
        self.filename = None
        self.path = path
        if path != "":
            self.load_image(os.path.join(path, name))
            self.filename = name
            self.path = path
        else:
            self.pixels = np.zeros((height, width))

    def load_image(self, name):
        "load image from forwarded path"
        im = Image.open(name).convert('LA')
        self.pixels = np.array(im).astype('f4')[:,:,0]
        [self.height, self.width] = self.pixels.shape

    def save_image(self, path, suffix=None):
        "save image"
        if not suffix:
            save_procedure(self.pixels, path, self.filename)
        else:
            save_procedure(self.pixels, path, self.filename, suffix)

    def show_image(self):
        "show image"
        pillow_image = Image.fromarray(self.pixels)
        pillow_image.show()
        pillow_image.close()

    def avg_pool(self, ratio):
        "lower dimensions of picture by certain ratio"
        new_picture = Picture(self.width//ratio, self.height//ratio)
        new_picture.filename = self.filename
        new_picture.pool_factor = ratio
        new_picture.pixels = np.array(Image.fromarray(self.pixels).resize((new_picture.width, new_picture.height)))
        return new_picture

    def local_normalization(self):
        "performs local normalization before MOSSE filtration"
        px = self.pixels
        px = px - np.min(px)
        px = px / np.max(px)
        px = px * 255
        EX = scipy.ndimage.gaussian_filter(px, (9,5))
        EX2 = scipy.ndimage.gaussian_filter(px**2, (9,5))
        h = (px - EX) / np.sqrt(EX2 - EX**2 + 0.01)
        new = Picture(self.width, self.height)
        new.pixels = h
        new.path, new.filename, new.pool_factor = self.path, self.filename, self.pool_factor
        return new

    def final_visualisation(self, bs, groups, save=False, show=False, path=None, subtype="", text=None):
        "final visualization, creates image containing fitted ladder and annotated bands"
        intern_offsets, ladder_points = bs.return_data_for_visualisation()
        ladder_offsets = np.copy(intern_offsets)
        max_val = max(bs.num_values)
        anotated_points = []
        grouped_points = []
        for group in groups:
            ap, off = bs.evaluate_group(group)
            lane_data = []
            temp = []
            lane_data.append(ap[0][0])
            for i in ap:
                temp.append(i[2])
            temp.sort()
            temp = temp[::-1]
            grouped_points.append(lane_data + temp)
            intern_offsets.append(off)
            anotated_points += ap
        intern_offsets.sort(key=lambda x: x[1])
        left = 0
        pixels = np.copy(self.pixels)
        for i in range(len(intern_offsets)-1):
            right = (intern_offsets[i+1][1] + intern_offsets[i][1]) // 2
            self.shift_panel(pixels, intern_offsets[i][0], left, right)
            left = right
        self.shift_panel(pixels, intern_offsets[-1][0], left, self.width)
        if not text is None:
            pixels = np.pad(pixels,((0,32*(1+len(text))),(0,0)),mode="constant")
        r = np.copy(pixels)
        g = np.copy(pixels)
        b = np.copy(pixels)
        selected_extremes = [anotated_points, ladder_points]
        for i in range(2):
            for j in range(len(selected_extremes[i])):
                if i==0 and selected_extremes[i][j][2] >= max_val:
                    continue
                base_x = selected_extremes[i][j][0]
                base_y = selected_extremes[i][j][1]
                for dx in range(-3, 4):
                    for dy in range(-3, 4):
                        x = base_x + dx
                        y = base_y + dy
                        if not is_in_image(x, y, r.shape):
                            continue
                        r[y, x] = 0
                        g[y, x] = 0
                        b[y, x] = 0
                        if i == 1:
                            g[y, x] = 255
                        else:
                            r[y, x] = 255
        rgbs = np.array([r, g, b]).astype(int).transpose([1, 2, 0]).astype('uint8')
        p = Image.fromarray(rgbs, 'RGB')
        for ap in anotated_points:
            if ap[2] >= max(bs.num_values):
                continue
            draw = ImageDraw.Draw(p)
            # font = ImageFont.truetype(<font-file>, <font-size>)
            font = ImageFont.truetype("font.ttf", 26)
            draw.text((int(ap[0] + 5), int(ap[1] - 4)), str(ap[2]), (255, 60, 60), font=font)
        for i in range(bs.ys.shape[0]):
            for j in range(bs.ys.shape[1]):
                draw = ImageDraw.Draw(p)
                # font = ImageFont.truetype(<font-file>, <font-size>)
                font = ImageFont.truetype("font.ttf", 20)
                draw.text((int(bs.xs[j]) + 5, int(bs.ys[i,j]-4 + ladder_offsets[j,0])), str(bs.num_values[i]), (100, 255, 100), font=font)
        if not text is None:
            self.write_legend_to_image(p, text, pixels.shape)
        if save:
            if path is None:
                path = self.path
            filename = self.filename[:len(self.filename) - 4] + "_" + subtype + "_fv.png"
            name = os.path.join(path, filename)
            p.save(name)
            p.close()
        elif show:
            p.show()
            p.close()
        else:
            grouped_points.sort()
            return p, grouped_points

    def shift_panel(self, pixels, offset, left_x, right_x):
        "support function, shift image so first line of bands, corresponding to largest size, starts on same y coordinate in image"
        offset = -offset
        new_panel = pixels[max(0, int(offset)):min(self.height, self.height + offset),left_x:right_x]
        pad = self.height - new_panel.shape[0]
        if offset > 0:
            new_panel = np.pad(new_panel,((0,pad),(0,0)), mode="constant")
        else:
            new_panel = np.pad(new_panel,((pad,0),(0,0)), mode="constant")
        pixels[:, left_x:right_x] = new_panel

    def write_legend_to_image(self,p, text, shape):
        "write legend to image under the image"
        colors = [(0,255,0),(255,0,0),(0,0,255),(255,255,0)]
        x = 50
        y = shape[0] - (len(text)+1) * 30
        for i in range(len(text)):
            draw = ImageDraw.Draw(p)
            for ii in range(-4,5):
                for ij in range(-4,5):
                    pass
                    draw.point((x+ii-10,y+ij+14),fill=colors[i])
            # font = ImageFont.truetype(<font-file>, <font-size>)
            font = ImageFont.truetype("font.ttf", 30)
            draw.text((x+7, y), text[i], colors[i], font=font)
            y+=30

    def create_rgb_with_marks(self, selected_extremes, save=False, show=False, path="", subtype="",text=None):
        "visualize results of detection with RGB marks"
        pixels = np.copy(self.pixels)
        if not text is None:
            pixels = np.pad(pixels,((0,32*(1+len(text))),(0,0)),mode="constant")
        r = np.copy(pixels)
        g = np.copy(pixels)
        b = np.copy(pixels)
        for i in range(1,-1,-1):
            for j in range(len(selected_extremes[i])):
                base_x = selected_extremes[i][j][0]
                base_y = selected_extremes[i][j][1]
                v = None
                if len(selected_extremes[i][j]) > 2:
                    v = selected_extremes[i][j][2]
                for dx in range(-3,4):
                    for dy in range(-3,4):
                        x = int(base_x + dx)
                        y = int(base_y + dy)
                        if not is_in_image(x,y,r.shape):
                            continue
                        r[y, x] = 0
                        g[y, x] = 0
                        b[y, x] = 0
                        if i == 0:
                            if v is None:
                                g[y,x] = 255
                            elif v == 0:
                                g[y,x] = 255
                                r[y,x] = 255
                            else:
                                g[y,x] = 255
                        else:
                            if v is None or v ==0:
                                r[y,x] = 255
                            else:
                                b[y,x] = int(255*v)

        rgbs = np.array([r,g,b]).astype(int).transpose([1,2,0]).astype('uint8')
        p = Image.fromarray(rgbs,'RGB')
        if not text is None:
            self.write_legend_to_image(p, text,pixels.shape)
        if save:
            filename = self.filename[:len(self.filename)-4] + "_" + subtype + "_nd.png"
            name = os.path.join(path, filename)
            p.save(name)
        elif show:
            p.show()
        return p

    def visualise_lines(self, lines, save=False, path=""):
        "visualise lines and ladders"
        r = self.pixels.copy()
        g = self.pixels.copy()
        b = self.pixels.copy()
        for t in range(2):
            for x in lines[t]:
                for i in range(self.height):
                    for j in range(-1,2):
                        b[i, x+j] = 0
                        if t==0:
                            r[i,x+j] = 255
                            g[i,x+j] = 0
                        else:
                            r[i,x+j] = 0
                            g[i,x+j] = 255
        rgbs = np.array((r,g,b)).astype(int).transpose([1,2,0]).astype('uint8')
        p = Image.fromarray(rgbs,'RGB')
        if save:
            filename = self.filename[:len(self.filename)-4] + "_lines.png"
            name = os.path.join(path, filename)
            p.save(name)
        else:
            p.show()
        p.close()

    def pixel_stretching(self):
        "support function, stretches pixel values to use entire 0-255 scale"
        self.pixels = self.pixels * (self.pixels > 0).astype(int)
        self.pixels = (self.pixels - np.min(self.pixels)) / (np.max(self.pixels - np.min(self.pixels))) * 255
        self.pixels = self.pixels.astype("float32")

    def compute_histogram(self):
        "returns histogram of image"
        return scipy.ndimage.histogram(self.pixels, 0, 255, 256)


def save_procedure(array, path, filename, suffix="transformed"):
    "support function for saving images"
    pillow_image = Image.fromarray(array.astype("uint8"))
    i = filename.find(".")
    filename = filename[:i] + "_" + suffix + ".png"
    print(filename)
    pillow_image.save(os.path.join(path, filename), "png")
    pillow_image.close()


def create_label_images(name):
    "create arrrays with marks positions from labeled image"
    g_t = 260
    g_l = 235
    o_l = -6
    o_t = 50
    data = np.array(Image.open(name)).astype('f4').transpose(2, 0, 1)
    r = (data[0] > o_l).astype(int) * (data[0] < o_t).astype(int)
    g = (data[1] > g_l).astype(int) * (data[1] < g_t).astype(int)
    b = (data[2] > o_l).astype(int) * (data[2] < o_t).astype(int)
    pixels = scipy.ndimage.binary_dilation(r * g * b, np.ones((3, 5))).astype(int)
    [height, width] = pixels.shape
    dirs = [(1, 0), (-1, 0), (0, 1), (-1, 0)]
    extremes = list()
    for y in range(height):
        for x in range(width):
            if pixels[y, x] < 0.5:
                continue
            i = 0
            x_agg = 0
            y_agg = 0
            mem = [(x, y)]
            while len(mem) > 0:
                x_, y_ = mem.pop()
                pixels[y_, x_] = 0
                i += 1
                x_agg += x_
                y_agg += y_
                for vx, vy in dirs:
                    new_x = x_ + vx
                    new_y = y_ + vy
                    if is_in_image(new_x, new_y, pixels.shape) and pixels[new_y, new_x] > 0.5 and (
                            new_x, new_y) not in mem:
                        mem.append((x_ + vx, y_ + vy))
            a = int(x_agg / i)
            b = int(y_agg / i)
            extremes.append([a, b])
    return extremes


def create_gaussian_markers(ex_array, shape, sigma, scale=1, negs=None):
    "perform gaussian blurring of marks for MOSSE filter"
    extremes = ex_array.astype(int)
    sigma = sigma * scale
    sigma = 2 * sigma**2
    new = np.zeros(shape)
    for s in range(extremes.shape[0]):
        y = int(extremes[s][1] * scale)
        x = int(extremes[s][0] * scale)
        for i in range(-8, 9):
            for j in range(-8, 9):
                new_x = x+i
                new_y = y+j
                if is_in_image(new_x, new_y, shape):
                    new[new_y, new_x] += np.exp(-(i ** 2) / sigma - 2*(j ** 2) / sigma)
    if negs is None:
        return new
    for s in range(len(negs)):
        y = int(negs[s][1] * scale)
        x = int(negs[s][0] * scale)
        for i in range(-8,9):
            for j in range(-8,9):
                new_x = x+i
                new_y = y+j
                if is_in_image(new_x, new_y, shape):
                    new[new_y, new_x] -= np.exp(-(i ** 2) / sigma - 2*(j ** 2) / sigma)
    return new


def is_in_image(x, y, shape):
    return 0 <= x < shape[1] and 0 <= y < shape[0]


def accuracy_computation(ext, saved_extremes, width=25):
    "compute accuracy of detection for detector evaluation, returns TP, FP, FN"
    extremes = ext.copy()

    def value_function(x_r, y_r, x, y):
        xx = (width - abs(x - x_r)) / width
        yy = (15 - abs(y_r - y)) / 15
        return np.sqrt(min(max(xx,0), max(yy,0)))
    FN = 0
    TP, FP = 0, 0
    found_with_values = []
    saved_with_res = []
    for i in range(saved_extremes.shape[0]):
        [x,y] = saved_extremes[i]
        max_index = -1
        max_val = 0
        for j in range(len(extremes)):
            val = value_function(x, y, extremes[j][0], extremes[j][1])
            if val > max_val:
                max_val = val
                max_index = j
        if max_index != -1:
            TP += max_val
            s = extremes.pop(max_index)
            found_with_values.append((s[0], s[1], max_val))
            saved_with_res.append((x, y, 1))
        else:
            saved_with_res.append((x,y,0))
            FN += 1
    FP += len(extremes)
    for s in extremes:
        found_with_values.append((s[0], s[1], 0))
    return TP / (TP + FP + FN + 0.1), [TP, FP, FN], [saved_with_res, found_with_values]
