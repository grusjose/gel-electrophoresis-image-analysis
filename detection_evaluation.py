import numpy as np
import scipy.ndimage
import picture
from PIL import Image
import matplotlib.pyplot as plt


def IOU(first_coord, second_coord):
    "function computing IOU of two rectangles"
    x_val = max(0, min(-first_coord[0][0] + second_coord[1][0], -second_coord[0][0] + first_coord[1][0]))
    y_val = max(0, min(-first_coord[0][1] + second_coord[1][1], -second_coord[0][1] + first_coord[1][1]))
    I = x_val * y_val
    U = (-first_coord[0][0] + first_coord[1][0]) * (-first_coord[0][1] + first_coord[1][1]) + \
            (-second_coord[0][0] + second_coord[1][0]) * (-second_coord[0][1] + second_coord[1][1]) - I
    return I / U


def locate_local_maxima(heatmap, threshold_multiplier=1, IOU_threshold=0.4, w=False):
    "locate bands by searching local maxima in image, return percentage of overlaps"
    data = heatmap.pixels.copy()
    data = scipy.ndimage.gaussian_filter(data, 2)
    data = data * (data > 0).astype(int)
    lim = (1 - threshold_multiplier) * np.percentile(data,85) + threshold_multiplier * np.max(data)
    data = data * (data > lim).astype(int)
    IOU_threshold_intern = 0.65
    bb_width = 160
    if w:
        bb_width = 240
    bb_height = 20
    proposed_bb = list()
    chosen_bb = list()
    marks = list()
    while True:
        val = np.amax(data)
        if lim > val:
            break
        result = np.argwhere(data >= val)
        for y, x in result:
            if data[y,x] < lim:
                continue
            else:
                flood_fill(data,x,y,lim)
                left_up = (x - bb_width//2, y - bb_height//2)
                right_down = (x + bb_width - bb_width//2, y + bb_height - bb_height//2)
                proposed_bb.append((left_up,right_down))
    small_overlaps = 0
    for bb in proposed_bb:
        IOU_test = True
        acc = 0
        for bbc in chosen_bb:
            acc += IOU(bb, bbc)
        if acc > IOU_threshold:
            small_overlaps += 1
        if acc > IOU_threshold_intern:
            IOU_test = False
        if IOU_test:
            chosen_bb.append(bb)
            x, y = (bb[0][0] + bb[1][0]) // 2,  (bb[0][1] + bb[1][1]) // 2
            marks.append((x, y))
    small_overlaps += 5
    if len(proposed_bb) < 10:
        small_overlaps = len(proposed_bb)
    return marks, small_overlaps / (len(proposed_bb) + small_overlaps + 0.1)


def flood_fill(array, start_x, start_y, lim):
    "support BFS for local maxima localisation"
    value = array[start_y, start_x]
    memory = [(start_x, start_y, value)]
    array[start_y,start_x] = 0
    while len(memory) > 0:
        x, y, val = memory.pop()
        for i in range(-1,2):
            for j in range(-1,2):
                new_x = x + j
                new_y = y + i
                if picture.is_in_image(new_x, new_y, array.shape):
                    new_val = array[new_y, new_x]
                    if lim < new_val <= 1.01 * val:
                        array[new_y, new_x] = 0
                        memory.append((new_x, new_y, new_val))


def locate_ladders_and_lines(extremes, image, ladder_coeff=0.65):
    "function differenting bands to groups, selecting groups that are ladders"
    impulses_of_extremes = np.zeros(image.width)
    for extreme in extremes:
        impulses_of_extremes[extreme[0]] += 1
    gauss_of_bands = scipy.ndimage.gaussian_filter1d(impulses_of_extremes, 12)
    rest_lims = []
    threshold = 0.008
    max_sum = 0
    while True:
        val = np.max(gauss_of_bands)
        if val < threshold:
            break
        x = np.argwhere(gauss_of_bands>=val)
        x = x[0,0]
        i, j = x-2, x+2
        while True:
            if i <= 0 or gauss_of_bands[i] > gauss_of_bands[i+2] or gauss_of_bands[i] == 0:
                break
            i -= 1
        while True:
            if j >= image.width-1 or gauss_of_bands[j] > gauss_of_bands[j-2] or gauss_of_bands[j] == 0:
                break
            j += 1
        rest_lims.append((i,j,np.sum(gauss_of_bands[max(0,i):min(j+1,image.width)])))
        if rest_lims[-1][2] > max_sum:
            max_sum = rest_lims[-1][2]
        gauss_of_bands[max(0,i):min(j+1,image.width)] = 0
    rest_lims.sort(key=lambda x:x[2])
    rest_lims = rest_lims[::-1]
    ladders_lims = []
    lim = ladder_coeff * max_sum
    for i in range(2,-1,-1):
        if len(rest_lims) <= i:
            continue
        if rest_lims[i][2] > lim:
            ladders_lims.append(rest_lims.pop(i))
    ladders = [[] for i in range(len(ladders_lims))]
    rest_points = [[] for i in range(len(rest_lims))]
    for extreme in extremes:
        included_in_ladder = False
        for i, lims in enumerate(ladders_lims):
            if lims[1] > extreme[0] > lims[0]:
                ladders[i].append(extreme)
                included_in_ladder = True
                break
        if not included_in_ladder:
            included_else = False
            for i, lims in enumerate(rest_lims):
                if lims[1] > extreme[0] > lims[0]:
                    rest_points[i].append(extreme)
                    included_else = True
                    break
            if not included_else:
                rest_points.append([extreme])

    angle = compute_rotation_from_ladders(ladders)
    new_image, new_ladders, new_points, lines = correct_angle(image, angle, ladders, rest_points)
    return new_image, new_ladders, new_points, lines, angle


def compute_rotation_from_ladders(ladders):
    "compute angle, between ladder line and y axis"
    accumulated = 0
    for ladder in ladders:
        matrix = np.array(ladder).astype(float)
        matrix[:,0] -= np.average(matrix[:,0])
        matrix[:,1] -= np.average(matrix[:,1])
        _, S, V = np.linalg.svd(matrix)
        V = np.transpose(V)
        if S[0] ** 2 > S[1] ** 2:
            v = V[:,0]
        else:
            v = V[:,1]
        accumulated -= np.arctan(v[0] / (v[1] + 0.001))
    return accumulated / (len(ladders) + 0.001)


def correct_angle(image, angle, ladders, points):
    "correct image and mark positions by given angle"
    r = Image.fromarray(image.pixels.copy())
    r = r.rotate(angle * 180 / np.pi)
    mid_X = image.width // 2
    mid_Y = image.height // 2
    new_points = []
    new_ladders = []
    lines = [[], []]
    for point_group in points:
        new_points.append([])
        if len(point_group) == 0:
            continue
        avg_x, nm = 0, 0
        for point in point_group:
            _x, _y = point[0] - mid_X, point[1] - mid_Y
            new_x = _x * np.cos(angle) + _y * np.sin(angle) + mid_X
            new_y = - _x * np.sin(angle) + _y * np.cos(angle) + mid_Y
            new_points[-1].append((int(new_x), int(new_y)))
            avg_x += new_x
            nm += 1
        lines[0].append(int(avg_x / nm))
    for ladder in ladders:
        if len(ladder) == 0:
            continue
        new_ladders.append([])
        avg_x, nm = 0, 0
        for point in ladder:
            _x, _y = point[0] - mid_X, point[1] - mid_Y
            new_x = _x * np.cos(angle) + _y * np.sin(angle) + mid_X
            new_y = -_x * np.sin(angle) + _y * np.cos(angle) + mid_Y
            new_ladders[-1].append((int(new_x), int(new_y)))
            avg_x += new_x
            nm += 1
        lines[1].append(int(avg_x / nm))
    new_pic = picture.Picture(image.width, image.height)
    new_pic.filename, new_pic.path, new_pic.pool_factor = image.filename, image.path, image.pool_factor
    new_pic.pixels = np.array(r).astype('f4')[:, :]
    return new_pic, new_ladders, new_points, lines
